const path = require('path');
const context = path.resolve(__dirname, 'src');

module.exports = (api) => {
  const env = api.cache(() => process.env.NODE_ENV);

  return {
    presets: [
      [
        require('@babel/preset-env'),
        {
          useBuiltIns: 'entry',
          corejs: 3,
        },
      ],
      require('@babel/preset-react'),
      require('@babel/preset-typescript'),
    ],
    plugins: [
      [
        require('react-hot-loader/babel'),
        {
          enable: [
            'common/nbsp/replaceNbsp',
            'common/punctuation/quote',
            'common/nbsp/beforeShortLastWord',
            'common/nbsp/afterShortWord',
          ],
        },
      ],
      require('babel-plugin-typograf'),
      require('@babel/plugin-proposal-export-default-from'),
      [require('@babel/plugin-proposal-decorators'), { legacy: true }],
      [require('@babel/plugin-proposal-class-properties', { loose: true })],
      require('@babel/plugin-transform-async-to-generator'),
      [
        require('babel-plugin-styled-components'),
        {
          displayName: true,
        },
      ],
    ],
  };
};
