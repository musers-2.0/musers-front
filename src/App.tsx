import * as React from 'react';
import { Provider } from 'mobx-react';
import { BrowserRouter as Router } from 'react-router-dom';

import Root from 'pages/Root';
import stores from 'store/index';

const App: React.FC = () => {
  return (
    <Provider {...stores}>
      <Router>
        <Root />
      </Router>
    </Provider>
  );
};

export default App;
