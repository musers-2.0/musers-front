import * as React from 'react';

import { ArrowProps } from './types';
import { ArrowImage, ArrowWrapper, Wrapper } from './styles';

const Arrow: React.FC<ArrowProps> = (props: ArrowProps) => {
  return (
    <Wrapper {...props}>
      <ArrowWrapper>
        <ArrowImage />
      </ArrowWrapper>
    </Wrapper>
  );
};

Arrow.defaultProps = {
  right: false,
};

export default React.memo(Arrow);
