import styled, { css } from 'styled-components';

import { COLORS } from 'styles/vars';
import ArrowIcon from 'img/arrow.component.svg';

import { ArrowProps } from './types';

export const Wrapper = styled.div<ArrowProps>`
  width: 5.6rem;
  height: 5.6rem;
  border-radius: 50%;
  background: ${COLORS.SURFACE};
  color: ${COLORS.DIM_TEXT};
  z-index: 100;
  transition: color 0.3s linear;

  &::before {
    display: none;
  }

  &:hover {
    background: ${COLORS.SURFACE};
    color: ${COLORS.TEXT};
  }

  ${({ right = false }: ArrowProps) =>
    right &&
    css`
      transform: rotate(180deg) translateY(50%);
    `}
`;

export const ArrowWrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export const ArrowImage = styled(ArrowIcon)`
  width: 1.2rem;
  height: 2rem;
`;
