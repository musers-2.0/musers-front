export interface ArrowProps {
  className?: string;
  style?: any;
  onClick?: () => void;
  right?: boolean;
}
