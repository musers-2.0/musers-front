import * as React from 'react';
import Slider from 'react-slick';

import Arrow from 'components/Arrow';

interface Props {
  children: React.ReactNode[];
  slidesToShow?: number;
  slidesToScroll?: number;
}

const Carousel: React.FC<Props> = ({
  children,
  slidesToShow,
  slidesToScroll,
}: Props) => {
  return (
    <Slider
      dots={false}
      slidesToShow={slidesToShow}
      slidesToScroll={slidesToScroll}
      swipeToSlide
      prevArrow={<Arrow />}
      nextArrow={<Arrow right />}
    >
      {children}
    </Slider>
  );
};

Carousel.defaultProps = {
  slidesToShow: 2,
  slidesToScroll: 1,
};

export default React.memo(Carousel);
