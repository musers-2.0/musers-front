import * as React from 'react';
import { observer } from 'mobx-react';
import { Link } from 'react-router-dom';

import { ShortConcertType } from 'store/types';
import { getDayMonthAndTime, getDaysLeft } from 'utils/date';

import {
  CardImage,
  CardWrapper,
  ConcertInfo,
  ConcertTitle,
  DaysLeft,
  GoButton,
  Hint,
  InfoLine,
  RockIcon,
} from './styles';

const ConcertCard: React.FC<ShortConcertType> = ({
  shortTitle,
  place,
  startDate,
  id,
  image,
}: ShortConcertType) => {
  const daysLeft = getDaysLeft(startDate);

  return (
    <Link to={`/concert/${id}`}>
      <CardWrapper>
        <CardImage src={image} />
        <ConcertInfo>
          <ConcertTitle>{shortTitle}</ConcertTitle>
          {place?.title && <InfoLine>{place?.title}</InfoLine>}
          <InfoLine>{getDayMonthAndTime(startDate)}</InfoLine>
        </ConcertInfo>
        <Hint>
          {daysLeft >= 0 && (
            <DaysLeft>{daysLeft ? `${daysLeft} дн.` : 'Сегодня'}</DaysLeft>
          )}
          <GoButton>
            <RockIcon />
          </GoButton>
        </Hint>
      </CardWrapper>
    </Link>
  );
};

export default observer(ConcertCard);
