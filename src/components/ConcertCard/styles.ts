import styled from 'styled-components';

import mock from 'img/mock.jpg';
import Rock from 'img/rock.component.svg';
import { COLORS } from 'styles/vars';

export const CardWrapper = styled.div`
  position: relative;
  min-height: 15rem;
  display: flex;
  align-items: center;
  cursor: pointer;
  margin-bottom: 3.2rem;

  &::before {
    content: '';
    z-index: -1;
    position: absolute;
    left: 0.4rem;
    right: 0;
    top: 0;
    height: 100%;
    background: ${COLORS.SURFACE};
    border-radius: 2.4rem;
  }
`;

export const CardImage = styled.div<{ src: string }>`
  width: 10.8rem;
  height: 10.8rem;
  border-radius: 1.6rem;
  background: url(${({ src }) => src}) no-repeat center / cover;
`;

export const ConcertInfo = styled.div`
  padding: 2.8rem 2.5rem;
  height: 100%;
`;

export const ConcertTitle = styled.div`
  font-size: 2.4rem;
  line-height: 3.6rem;
  max-width: 35rem;
  font-weight: 500;
  margin-bottom: 0.8rem;
`;

export const InfoLine = styled.div`
  font-size: 1.6rem;
  line-height: 2rem;
  margin-bottom: 1.2rem;
  color: ${COLORS.DIM_TEXT};
  font-weight: 300;

  &:last-child {
    margin-bottom: 0;
  }
`;

export const Hint = styled.div`
  position: absolute;
  top: 1.2rem;
  right: 1.2rem;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const DaysLeft = styled.div`
  height: 3.6rem;
  padding: 0.8rem 1.2rem;
  border-radius: 2.4rem;
  background: ${COLORS.BACKGROUND};
  color: ${COLORS.DIM_TEXT};
  font-size: 1.2rem;
  margin-right: 0.4rem;
  display: flex;
  align-items: center;
`;

export const GoButton = styled.div`
  width: 3.6rem;
  height: 3.6rem;
  border-radius: 50%;
  background: ${COLORS.BACKGROUND};
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const RockIcon = styled(Rock)`
  width: 1.6rem;
  height: 1.6rem;
  color: ${COLORS.ORANGE};
`;
