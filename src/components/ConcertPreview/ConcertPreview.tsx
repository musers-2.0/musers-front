import * as React from 'react';
import { Link } from 'react-router-dom';

import { ShortConcertType } from 'store/types';
import { getDayMonthAndTime } from 'utils/date';
import pluralize from 'utils/pluralize';

import {
  InfoText,
  Artist,
  Background,
  Content,
  Image,
  LeftPart,
  RockLogo,
  VisitLogo,
  Visitor,
  Visitors,
} from './styles';
import { ConcertPreviewProps } from './types';

type Props = ConcertPreviewProps & ShortConcertType;

const ConcertPreview: React.FC<Props> = ({
  id,
  shortTitle,
  startDate,
  image,
  place,
  usersCount,
  users,
  hasPaddings,
}: Props) => {
  return (
    <Background hasPaddings={hasPaddings}>
      <Link to={`/concert/${id}`}>
        <Content>
          <LeftPart>
            <Image src={image} />
            <div>
              <Artist>{shortTitle}</Artist>
              <InfoText>
                {getDayMonthAndTime(startDate)}
                {` - ${place?.location || ''}`}
              </InfoText>
            </div>
          </LeftPart>
          <div>
            {users?.length ? (
              <>
                <Visitors>
                  <VisitLogo>
                    <RockLogo />
                  </VisitLogo>
                  {users.map((u) => (
                    <Visitor key={u.id} src={u.avatar} />
                  ))}
                </Visitors>
                <InfoText>
                  {pluralize(usersCount, [
                    'человек уже идут',
                    'человек уже идет',
                    'человека уже идут',
                  ])}
                </InfoText>
              </>
            ) : (
              <InfoText>Пока никто не идет :(</InfoText>
            )}
          </div>
        </Content>
      </Link>
    </Background>
  );
};

ConcertPreview.defaultProps = {
  hasPaddings: false,
};

export default React.memo(ConcertPreview);
