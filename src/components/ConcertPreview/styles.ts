import styled, { css } from 'styled-components';

import { ANIMATIONS, COLORS, MARGINS } from 'styles/vars';
import Rock from 'img/rock.component.svg';
import { Text, Title } from 'components/ui';

import { ConcertPreviewProps } from './types';

export const Background = styled.div<ConcertPreviewProps>`
  margin-bottom: 3rem;
  position: relative;
  height: 19.2rem;
  width: calc(100% - 3rem);
  cursor: pointer;
  transition: transform 0.3s linear;

  ${({ hasPaddings }: ConcertPreviewProps) =>
    hasPaddings
      ? css`
          padding: 0 ${MARGINS.SIDE};
          width: calc(100% - 3rem - ${MARGINS.SIDE} - ${MARGINS.SIDE});
        `
      : css`
          &:last-child {
            margin-bottom: 0;
          }
        `}

  &::before {
    content: '';
    position: absolute;
    height: 100%;
    width: 100%;
    background: ${COLORS.SURFACE};
    border-radius: 3.6rem;
    margin-left: 2.4rem;
  }

  &:hover {
    transform: scale(1.01);
  }
`;

export const Content = styled.div`
  position: absolute;
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const LeftPart = styled.div`
  display: flex;
  align-items: center;
`;

export const Image = styled.div<{ src: string }>`
  width: 15rem;
  height: 15rem;
  border-radius: 2.4rem;
  background: url(${({ src = '' }) => src}) no-repeat center / cover;
  margin-right: 2.4rem;
  flex-shrink: 0;
`;

export const Artist = styled(Title)`
  font-size: 3.2rem;
  padding-bottom: 1.6rem;
`;

export const InfoText = styled(Text)`
  font-size: 2rem;
  line-height: 2.4rem;
`;

export const Visitors = styled.div`
  display: flex;
  flex-direction: row-reverse;
  margin-bottom: 1.6rem;
`;

export const Visitor = styled.div<{ src: string }>`
  display: inline-block;
  width: 4.8rem;
  height: 4.8rem;
  border-radius: 50%;
  border: 0.4rem ${COLORS.SURFACE} solid;
  background: url(${({ src }) => src}) no-repeat center / cover;
  margin-left: -1.6rem;
`;

export const VisitLogo = styled.div`
  width: 4.8rem;
  height: 4.8rem;
  border-radius: 50%;
  background: ${COLORS.BACKGROUND};
  margin-left: -1.6rem;
  position: relative;
  z-index: 100;
`;

export const RockLogo = styled(Rock)`
  cursor: pointer;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  color: ${COLORS.ORANGE};
  width: 2rem;
  height: 2rem;
  ${ANIMATIONS.ORANGE_TO_PINK_HOVER};
`;
