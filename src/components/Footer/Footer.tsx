import * as React from 'react';

import { Logo } from 'components/ui';

import { Wrapper } from './styles';

const Footer: React.FC = () => {
  return (
    <Wrapper>
      <Logo isWhite>Musers</Logo>
      <div>© 2020 «Musers»</div>
    </Wrapper>
  );
};

export default React.memo(Footer);
