import styled from 'styled-components';

import { Text } from 'components/ui';
import { COLORS, MARGINS } from 'styles/vars';

export const Wrapper = styled(Text)`
  background: ${COLORS.SURFACE};
  padding: 1.6rem ${MARGINS.SIDE};
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
