import * as React from 'react';

import { CloseIcon, GradientHead, Text, Wrapper } from './styles';

interface Props {
  title: React.ReactNode;
  onClose?: null | (() => void);
  children: React.ReactNode;
}

const GradientHeadedBlock: React.FC<Props> = ({
  title,
  onClose,
  children,
}: Props) => {
  return (
    <Wrapper>
      <GradientHead>{title}</GradientHead>
      {onClose && <CloseIcon onClick={onClose} />}
      <Text>{children}</Text>
    </Wrapper>
  );
};

GradientHeadedBlock.defaultProps = {
  onClose: null,
};

export default React.memo(GradientHeadedBlock);
