import styled from 'styled-components';

import Close from 'img/close.component.svg';
import { ANIMATIONS, COLORS, GRADIENTS } from 'styles/vars';

export const Wrapper = styled.div`
  border-radius: 3.6rem;
  overflow: hidden;
  background: ${COLORS.SURFACE};
  text-align: center;
  position: relative;
  box-shadow: 0 2.4rem 5.4rem rgba(0, 0, 0, 0.25);
`;

export const GradientHead = styled.div`
  background: ${GRADIENTS.ORANGE_RADIAL};
  font-size: 3.2rem;
  line-height: 4rem;
  font-weight: 600;
  padding: 4rem;
`;

export const Text = styled.div`
  margin: 4rem;
  font-size: 2rem;
  line-height: 2.4rem;
  font-weight: 200;
`;

export const CloseIcon = styled(Close)`
  position: absolute;
  top: 2.5rem;
  right: 2.5rem;
  width: 3rem;
  height: 3rem;
  color: white;
  cursor: pointer;
  ${ANIMATIONS.SCALE_HOVER};
`;
