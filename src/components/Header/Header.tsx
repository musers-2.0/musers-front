import * as React from 'react';
import { observer } from 'mobx-react';
import { Link, useHistory, useLocation } from 'react-router-dom';

import { Logo } from 'components/ui';
import { useUserStore } from 'store/hooks';

import SearchBar from './components/SearchBar';
import LogoutIcon from './img/logout.component.svg';
import {
  Avatar,
  Count,
  Counter,
  HeaderButton,
  LoggedInContent,
  Logout,
  LogoutWrapper,
  RockIcon,
  Wrapper,
} from './styles';

const pagesWithTransparentHeader = ['/login'];

const Header: React.FC = () => {
  const { isAuthorized, currentUser, sendLogout } = useUserStore();

  const location = useLocation();
  const transparentHeader =
    pagesWithTransparentHeader.indexOf(location.pathname) !== -1 &&
    !isAuthorized;

  const history = useHistory();

  const onLogout = React.useCallback(() => {
    sendLogout();
    history.push('/');
  }, []);

  return (
    <Wrapper absolute={transparentHeader}>
      <Link to="/">
        <Logo>Musers</Logo>
      </Link>
      {!transparentHeader && (
        <>
          <SearchBar />
          {isAuthorized && currentUser ? (
            <LoggedInContent>
              <Counter>
                <RockIcon filled={currentUser.concertsCount > 0 ? 1 : 0} />
                <Count>{currentUser.concertsCount}</Count>
              </Counter>
              <Link to="/profile">
                <Avatar src={currentUser.avatar}>
                  <LogoutWrapper>
                    <Logout onClick={onLogout}>
                      <span>Выйти</span>
                      <LogoutIcon />
                    </Logout>
                  </LogoutWrapper>
                </Avatar>
              </Link>
            </LoggedInContent>
          ) : (
            <HeaderButton to="/login">Вход</HeaderButton>
          )}
        </>
      )}
    </Wrapper>
  );
};

export default observer(Header);
