import * as React from 'react';
import Select from 'react-select';
import { ValueType } from 'react-select/src/types';

import { observer } from 'mobx-react';
import { useHistory } from 'react-router';

import { useCommonDataStore } from 'store/hooks';
import { getDayAndMonth } from 'utils/date';

import { selectStyles } from './styles';

const SearchBar: React.FC = () => {
  const commonDataStore = useCommonDataStore();
  const history = useHistory();

  const [inputValue, setInputValue] = React.useState<string | null>(null);

  const onChange = (value: string) => setInputValue(value);

  React.useEffect(() => {
    if (inputValue) {
      commonDataStore.searchConcerts(inputValue);
    }
  }, [inputValue]);

  const options = commonDataStore.concertsSearchResult.map((c) => ({
    value: String(c.id),
    label: `${c.shortTitle}, ${getDayAndMonth(c.startDate)}`,
  }));

  const onConcertSelected = ({ value }: ValueType<any, any>) => {
    history.push(`/concert/${value}`);
  };

  return (
    <Select
      onChange={onConcertSelected}
      noOptionsMessage={() => 'Ничего не найдено'}
      styles={selectStyles}
      placeholder="Поиск концертов"
      isSearchable
      closeMenuOnSelect
      onInputChange={onChange}
      isLoading={commonDataStore.isSearching}
      options={options}
    />
  );
};

export default observer(SearchBar);
