import { CSSObject } from '@emotion/serialize';
import { OptionProps } from 'react-select/src/components/Option';

import { COLORS } from 'styles/vars';

export const selectStyles = {
  input: (provided: CSSObject): CSSObject => ({
    ...provided,
    fontFamily: 'Gilroy, sans-serif',
    marginLeft: '0.8rem',
    color: COLORS.GRAY,
    border: 'none',
    width: 'calc(32rem - 4rem)',
    backgroundColor: 'transparent',
  }),
  control: (): CSSObject => ({
    width: '32rem',
    height: '4.8rem',
    padding: '1.6rem',
    borderRadius: '2rem',
    backgroundColor: COLORS.BACKGROUND,
    display: 'flex',
    alignItems: 'center',
  }),
  valueContainer: (provided: CSSObject): CSSObject => ({
    ...provided,
    fontSize: '1.6rem',
    color: COLORS.TEXT,
  }),
  singleValue: (provided: CSSObject): CSSObject => ({
    ...provided,
    maxWidth: 'calc(100% - 3.6rem)',
  }),
  indicatorsContainer: (): CSSObject => ({ display: 'none' }),
  indicatorSeparator: (): CSSObject => ({ display: 'none' }),
  menu: (provided: CSSObject): CSSObject => ({
    ...provided,
    backgroundColor: COLORS.BACKGROUND,
    borderRadius: '2rem',
    overflow: 'hidden',
    fontSize: '1.6rem',
    color: COLORS.DIM_TEXT,
  }),
  option: (provided: CSSObject, state: OptionProps<any, any, any>) => ({
    ...provided,
    backgroundColor:
      state.isFocused || state.isSelected ? COLORS.SURFACE : 'transparent',
    color: state.isSelected ? COLORS.TEXT : COLORS.DIM_TEXT,
    padding: '1.2rem',
  }),
};
