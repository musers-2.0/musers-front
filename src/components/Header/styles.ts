import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';

import { ANIMATIONS, COLORS, MARGINS } from 'styles/vars';
import Rock from 'img/rock.component.svg';
import mock from 'img/mock.jpg';

export const Wrapper = styled.div<{ absolute?: boolean }>`
  width: 100%;
  height: 8rem;
  padding: 0 ${MARGINS.SIDE};
  display: flex;
  align-items: center;
  justify-content: space-between;
  background: ${({ absolute = false }) =>
    absolute ? 'transparent' : COLORS.SURFACE};
  ${({ absolute = false }) =>
    absolute &&
    css`
      position: absolute;
      z-index: 1000;
    `}
`;

export const HeaderButton = styled(Link)`
  font-size: 1.6rem;
  color: ${COLORS.DIM_TEXT};
  cursor: pointer;

  ${ANIMATIONS.TEXT_HOVER}
`;

export const LoggedInContent = styled.div`
  display: flex;
  align-items: center;
`;

export const Counter = styled.div`
  display: inline-flex;
  align-items: center;
  height: 4.8rem;
  background: ${COLORS.BACKGROUND};
  padding: 1.6rem;
  border-radius: 2.4rem;
  color: ${COLORS.DIM_TEXT};
  margin-right: 1.6rem;
`;

export const RockIcon = styled(Rock)<{ filled: number }>`
  width: 2rem;
  height: 2rem;
  margin-right: 0.8rem;

  ${({ filled }: { filled: number }) =>
    filled ? ANIMATIONS.ORANGE_TO_PINK_HOVER : `color: ${COLORS.DIM_TEXT}`};
`;

export const Count = styled.div`
  font-size: 1.6rem;
`;

export const LogoutWrapper = styled.div`
  display: none;
  position: absolute;
  bottom: 0;
  right: 0;
  transform: translate(0, 100%);
  z-index: 1000;
`;

export const Logout = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  color: ${COLORS.DIM_TEXT};
  margin-top: 2.8rem;
  width: 12rem;
  background: ${COLORS.SURFACE};
  padding: 1.6rem;
  border-radius: 1.8rem;
  font-size: 1.6rem;

  ${ANIMATIONS.TEXT_HOVER}
`;

export const Avatar = styled.div<{ src: string }>`
  width: 4.8rem;
  height: 4.8rem;
  border-radius: 1.8rem;
  background: url(${({ src = '' }) => src}) no-repeat center / cover;
  cursor: pointer;
  position: relative;

  &:hover {
    ${LogoutWrapper} {
      display: flex;
    }
  }
`;
