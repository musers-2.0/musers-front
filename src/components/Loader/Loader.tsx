import * as React from 'react';
import { Transition } from 'react-transition-group';

import { Icon, LoaderBackground, LoaderWrapper } from './styles';

interface Props {
  show: boolean;
  isFull?: boolean;
}

const transitionStyles = {
  entering: { opacity: 1 },
  entered: { opacity: 1 },
  exiting: { opacity: 0 },
  exited: { opacity: 0 },
};

const Loader: React.FC<Props> = ({ show, isFull }: Props) => {
  return (
    <Transition in={show} timeout={300} mountOnEnter unmountOnExit>
      {(state: any) => (
        <LoaderBackground
          colored={isFull}
          style={{ ...transitionStyles[state] }}
        >
          <LoaderWrapper>
            <Icon />
            <Icon />
            <Icon />
          </LoaderWrapper>
        </LoaderBackground>
      )}
    </Transition>
  );
};

Loader.defaultProps = {
  isFull: false,
};

export default React.memo(Loader);
