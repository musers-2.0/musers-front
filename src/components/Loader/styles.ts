import styled, { css, keyframes } from 'styled-components';

import { COLORS } from 'styles/vars';

import first from './img/first.svg';
import second from './img/second.svg';
import third from './img/third.svg';

export const LoaderBackground = styled.div<{ colored?: boolean }>`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 1000;
  ${(props) =>
    props.colored
      ? css`
          background: ${COLORS.BACKGROUND};
        `
      : css`
          pointer-events: none;
        `}
`;

export const LoaderWrapper = styled.div`
  position: absolute;
  top: 48%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 16rem;
  height: 16rem;
`;

const flyOut = (x: number, y: number, scale: number) =>
  keyframes`
    0% {
      transform: translate3d(${x}rem, ${y}rem, 0) scale(${scale});
    }
    100% {
      transform: translate3d(0, 0,0) scale(${scale});
    }`;

export const Icon = styled.div`
  position: absolute;
  width: 10rem;
  height: 10rem;
  transform: translate3d(0, 0, 0);

  &:nth-child(1) {
    top: 0;
    left: 0;
    background: url(${third}) no-repeat center / contain;
    animation: ${flyOut(0, 4, 1.2)} 1.5s ease alternate infinite;
  }

  &:nth-child(2) {
    left: 45%;
    top: 55%;
    background: url(${second}) no-repeat center / contain;
    animation: ${flyOut(-8, -4, 1.1)} 1.5s ease alternate infinite;
  }

  &:nth-child(3) {
    left: -60%;
    top: 60%;
    background: url(${first}) no-repeat center / contain;
    animation: ${flyOut(8, -4, 1)} 1.5s ease alternate infinite;
  }
`;
