import * as React from 'react';

import { Content, Wrapper } from './styles';

interface Props {
  children: React.ReactNode;
}

const Modal: React.FC<Props> = ({ children }: Props) => {
  return (
    <Wrapper>
      <Content>{children}</Content>
    </Wrapper>
  );
};

export default React.memo(Modal);
