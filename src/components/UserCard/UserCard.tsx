import * as React from 'react';

// import MapIcon from 'img/address.component.svg';
import RockIcon from 'img/rock.component.svg';
import VkIcon from 'img/vk.component.svg';
import { UserType } from 'store/types';
import pluralize from 'utils/pluralize';

import {
  CardWrapper,
  Image,
  Info,
  InfoIcon,
  InfoString,
  Title,
} from './styles';

const UserCard: React.FC<UserType> = ({
  firstName,
  lastName,
  avatar,
  domain,
  age,
  concertsCount,
}: UserType) => {
  return (
    <CardWrapper>
      <Image src={avatar} />
      <Info>
        <Title>
          {firstName} {lastName}, {age}
        </Title>
        {/*<InfoString>*/}
        {/*  <InfoIcon>*/}
        {/*    <MapIcon />*/}
        {/*  </InfoIcon>*/}
        {/*<div>{city}</div>*/}
        {/*</InfoString>*/}
        <InfoString>
          <InfoIcon>
            <RockIcon />
          </InfoIcon>
          {concertsCount > 0 ? (
            <div>
              Собирается на{' '}
              {pluralize(concertsCount, ['концертов', 'концерт', 'концерта'])}
            </div>
          ) : (
            <div>Пока не идет на концерты</div>
          )}
        </InfoString>
        {domain && (
          <InfoString link>
            <InfoIcon>
              <VkIcon />
            </InfoIcon>
            <a href={`https://vk.com/${domain}`} target="_blank">
              {domain}
            </a>
          </InfoString>
        )}
      </Info>
    </CardWrapper>
  );
};

export default React.memo(UserCard);
