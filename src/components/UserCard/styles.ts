import styled, { css } from 'styled-components';

import { COLORS, GRADIENTS } from 'styles/vars';

export const CardWrapper = styled.div`
  width: 32rem;
  min-height: 48rem;
  border-radius: 2.4rem;
  overflow: hidden;
  background: ${COLORS.SURFACE};
  box-shadow: 0 14px 64px rgba(29, 31, 38, 0.5);
`;

export const Image = styled.div<{ src: string }>`
  height: 32rem;
  background: url(${({ src }) => src}) no-repeat center / cover;
  position: relative;

  &::after {
    content: '';
    position: absolute;
    width: 100%;
    height: 50%;
    bottom: 0;
    left: 0;
    background: ${GRADIENTS.SMOKE_GRADIENT};
  }
`;

export const Info = styled.div`
  padding: 2.5rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const Title = styled.div`
  font-size: 2.4rem;
  margin-bottom: 1.2rem;
`;

export const InfoString = styled.div<{ link?: boolean }>`
  font-size: 1.6rem;
  line-height: 2rem;
  color: ${COLORS.DIM_TEXT};
  display: flex;
  align-items: center;
  font-weight: 200;

  ${({ link = false }) =>
    link &&
    css`
      text-decoration: underline;
      cursor: pointer;
    `}
`;

export const InfoIcon = styled.div`
  color: ${COLORS.DIM_TEXT};
  width: 1.2rem;
  height: 1.2rem;
  margin-right: 0.8rem;
  display: flex;
  align-items: center;
  justify-content: center;
`;
