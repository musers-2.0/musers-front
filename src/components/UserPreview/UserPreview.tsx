import * as React from 'react';
import { Link } from 'react-router-dom';

import { UserType } from 'store/types';
import pluralize from 'utils/pluralize';

import {
  ConcertsCount,
  Info,
  Name,
  RockLogo,
  UserAvatar,
  UserContent,
  UserPreviewWrapper,
} from './styles';

const UserPreview: React.FC<UserType> = ({
  id,
  firstName,
  avatar,
  age,
  concertsCount,
}: UserType) => {
  return (
    <Link to={`user/${id}`}>
      <UserPreviewWrapper>
        <UserAvatar src={avatar}>
          {concertsCount > 0 && (
            <ConcertsCount>
              <RockLogo />
              {concertsCount}
            </ConcertsCount>
          )}
        </UserAvatar>
        <UserContent>
          <Name>{firstName}</Name>
          {age && <Info>{pluralize(age, ['лет', 'год', 'года'])}</Info>}
        </UserContent>
      </UserPreviewWrapper>
    </Link>
  );
};

export default React.memo(UserPreview);
