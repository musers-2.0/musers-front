import styled from 'styled-components';

import { ANIMATIONS, COLORS, GRADIENTS } from 'styles/vars';
import Rock from 'img/rock.component.svg';

export const UserPreviewWrapper = styled.div`
  width: 24rem;
  height: 32rem;
  border-radius: 2.4rem;
  background: ${COLORS.SURFACE};
  overflow: hidden;
`;

export const UserAvatar = styled.div<{ src: string }>`
  height: 24rem;
  width: 100%;
  background: url(${({ src }) => src}) no-repeat center / cover;
  position: relative;

  &::after {
    content: '';
    position: absolute;
    width: 100%;
    height: 50%;
    bottom: 0;
    left: 0;
    background: ${GRADIENTS.SMOKE_GRADIENT};
  }
`;

export const UserContent = styled.div`
  height: calc(100% - 24rem);
  padding: 1.4rem 2.4rem;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const Name = styled.div`
  font-size: 2.4rem;
  font-weight: 600;
`;

export const Info = styled.div`
  color: ${COLORS.DIM_TEXT};
  font-size: 1.6rem;
  line-height: 2rem;
`;

export const ConcertsCount = styled.div`
  position: absolute;
  z-index: 100;
  bottom: 0;
  right: 1rem;
  background: ${COLORS.BACKGROUND};
  padding: 1rem;
  border-radius: 2.4rem;
  font-size: 1.2rem;
  color: ${COLORS.DIM_TEXT};
  display: flex;
  align-items: center;
`;

export const RockLogo = styled(Rock)`
  color: ${COLORS.ORANGE};
  width: 1.2rem;
  height: 1.2rem;
  ${ANIMATIONS.ORANGE_TO_PINK_HOVER};
  margin-right: 0.4rem;
`;
