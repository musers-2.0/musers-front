import styled, { css, keyframes } from 'styled-components';

import { ANIMATIONS, COLORS, GRADIENTS } from 'styles/vars';
import logo from 'img/logo.svg';
import whiteLogo from 'img/logo_white.svg';

export const Title = styled.div`
  font-size: 4.2rem;
  font-weight: 500;
  padding-bottom: 2rem;
  line-height: 1em;
`;

export const Text = styled.div`
  font-size: 1.6rem;
  font-weight: 200;
  color: ${COLORS.DIM_TEXT};
`;

export const Button = styled.div`
  cursor: pointer;
  display: inline-block;
  padding: 1.2rem 2rem;
  font-size: 2rem;
  line-height: 2.4rem;
  border-radius: 1.6rem;
`;

export const GradientButton = styled(Button)`
  background: ${GRADIENTS.PINK_RADIAL}, ${GRADIENTS.PINK_LINEAR};
  background-size: 200% 100%;
  ${ANIMATIONS.GRADIENT_HOVER};
`;

const bounce = keyframes`
  0%, 50%, 100% {
    transform: translateY(0);
  }
  
  25%, 75% {
    transform: translateY(5%);
  }
`;

export const Logo = styled.div<{ isWhite?: boolean }>`
  display: inline-flex;
  align-items: center;
  font-size: 2.2rem;
  font-weight: 400;
  cursor: pointer;

  &::before {
    content: '';
    display: inline-block;
    width: 4.5rem;
    height: 4.8rem;
    margin-right: 0.8rem;
    background: url(${({ isWhite = false }) => (isWhite ? whiteLogo : logo)})
      no-repeat center / contain;
  }

  ${({ isWhite = false }) =>
    isWhite
      ? ANIMATIONS.TEXT_HOVER
      : css`
          &:hover {
            &::before {
              animation: ${bounce} 0.5s ease-in-out;
            }
          }
        `}
`;

export const UnderlinedText = styled(Text)`
  text-decoration: underline;
  cursor: pointer;

  ${ANIMATIONS.TEXT_HOVER}
`;
