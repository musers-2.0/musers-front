export const oauthUrl =
  'https://oauth.vk.com/authorize?client_id=7782052&redirect_uri=https://musers.me/api/user/oauth&display=page&scope=65546&response_type=code';

export const apiUrl = 'api/';

export default {
  userList: `${apiUrl}user/list`,
  userCurrent: `${apiUrl}user/current`,
  userLogout: `${apiUrl}user/logout`,
  userOauth: `${apiUrl}user/oauth`,
  userGet: `${apiUrl}user/get`,
  recommendedUsers: `${apiUrl}user/list_recommended`,
  concertList: `${apiUrl}concert/list`,
  concertGet: `${apiUrl}concert/get`,
  concertToggle: `${apiUrl}concert/toggle`,
  recommendedConcerts: `${apiUrl}concert/list_recommended`,
};
