import 'regenerator-runtime/runtime';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import markup from 'utils/markup';
import 'styles/styles.scss';

import App from './App';

markup.init();

const init = () => {
  window.onload = () => {
    ReactDOM.render(<App />, document.getElementById('app'));
  };
};

init();
