import * as React from 'react';
import { useParams } from 'react-router';
import { observer } from 'mobx-react';

import { useCommonDataStore } from 'store/hooks';
import Loader from 'components/Loader';

import ConcertHead from './components/ConcertHead';
import ConcertInfo from './components/ConcertInfo';
// import AuthRequiredModal from './components/AuthRequiredModal';

const Concert: React.FC = () => {
  const commonDataStore = useCommonDataStore();

  const { id } = useParams<{ id: string }>();

  React.useEffect(() => {
    commonDataStore.fetchConcert(id);
  }, []);

  return (
    <>
      <Loader isFull show={commonDataStore.isLoading} />
      {!commonDataStore.isLoading && commonDataStore.currentConcert && (
        <>
          <ConcertHead />
          <ConcertInfo />
        </>
      )}
    </>
  );
};

export default observer(Concert);
