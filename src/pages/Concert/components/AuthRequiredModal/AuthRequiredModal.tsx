import * as React from 'react';

import Modal from 'components/Modal';
import GradientHeadedBlock from 'components/GradientHeadedBlock';

import { AuthButton, VkIcon } from './styles';

interface Props {
  onClose: VoidFunction;
}

const AuthRequiredModal: React.FC<Props> = ({ onClose }: Props) => {
  return (
    <Modal>
      <GradientHeadedBlock
        title={
          <>
            Подключите учетную
            <br />
            запись
          </>
        }
        onClose={onClose}
      >
        <div>
          Для более эффективного поиска концертов
          <br />
          подключите аккаунт ВКонтакте
        </div>
        <AuthButton>
          <VkIcon />
          <div>Авторизоваться</div>
        </AuthButton>
      </GradientHeadedBlock>
    </Modal>
  );
};

export default React.memo(AuthRequiredModal);
