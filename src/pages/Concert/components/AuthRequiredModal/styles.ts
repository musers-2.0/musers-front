import styled from 'styled-components';

import { Button } from 'components/ui';
import { ANIMATIONS, COLORS } from 'styles/vars';
import VK from 'img/vk.component.svg';

export const AuthButton = styled(Button)`
  background: ${COLORS.BACKGROUND};
  display: inline-flex;
  align-items: center;
  justify-content: center;
  margin-top: 3.2rem;
  box-shadow: 0 4px 14px rgba(32, 34, 41, 0.25),
    0 14px 54px rgba(32, 34, 41, 0.7);
  padding: 1.6rem;

  ${ANIMATIONS.SCALE_HOVER};
`;

export const VkIcon = styled(VK)`
  width: 2.7rem;
  height: 1.6rem;
  margin-right: 1rem;
`;
