import * as React from 'react';
import { observer } from 'mobx-react';

import { Title } from 'components/ui';
import { useCommonDataStore, useUserStore } from 'store/hooks';
import { getDayMonthAndTime } from 'utils/date';

import AuthRequiredModal from '../AuthRequiredModal';

import { Content, GoButton, Info, RockLogo, Wrapper } from './styles';

const ConcertHead: React.FC = () => {
  const { currentConcert } = useCommonDataStore();
  const { sendConcertToggle, isAuthorized } = useUserStore();

  const [authRequired, setAuthRequired] = React.useState(false);

  if (!currentConcert) {
    return null;
  }

  const onGoClick = () => {
    if (!isAuthorized) {
      setAuthRequired(true);
      return;
    }

    sendConcertToggle(currentConcert.id);
  };

  return (
    <>
      <Wrapper src={currentConcert.image}>
        <Content>
          <Title>{currentConcert.shortTitle}</Title>
          {currentConcert.title && <Info>{currentConcert.title}</Info>}
          <Info>
            {currentConcert.place?.title && `${currentConcert.place?.title}, `}
            {currentConcert.startDate &&
              getDayMonthAndTime(currentConcert.startDate)}
          </Info>
          <GoButton
            onClick={onGoClick}
            active={Boolean(currentConcert.isGoing)}
          >
            <RockLogo />
            <div>{currentConcert.isGoing ? 'Я иду' : 'Я пойду'}</div>
          </GoButton>
        </Content>
      </Wrapper>
      {authRequired && (
        <AuthRequiredModal onClose={() => setAuthRequired(false)} />
      )}
    </>
  );
};

export default observer(ConcertHead);
