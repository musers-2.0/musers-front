import styled, { css } from 'styled-components';

import { bottomGradientOverlay } from 'styles/mixins';
import { MARGINS } from 'styles/vars';
import { GradientButton, Text } from 'components/ui';
import Rock from 'img/rock.component.svg';

export const Wrapper = styled.div<{ src: string }>`
  height: 50rem;
  background: url(${({ src = '' }) => src}) no-repeat top / cover;
  padding: 0 ${MARGINS.SIDE};
  ${bottomGradientOverlay};
`;

export const Content = styled.div`
  position: absolute;
  left: ${MARGINS.SIDE};
  bottom: 0;
`;

export const Info = styled(Text)`
  font-size: 2rem;
  margin-bottom: 3rem;
`;

export const GoButton = styled(GradientButton)<{ active: boolean }>`
  color: white;
  display: inline-flex;

  ${({ active }) =>
    active &&
    css`
      background: radial-gradient(
        87.92% 244.7% at 86.88% 12.08%,
        #74cda3 3.74%,
        #2ab5a9 100%
      );
    `}
`;

export const RockLogo = styled(Rock)`
  width: 2.4rem;
  height: 2.4rem;
  margin-right: 1rem;
`;
