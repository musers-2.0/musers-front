import * as React from 'react';
import { YMaps, Map, Placemark } from 'react-yandex-maps';

import { useCommonDataStore } from 'store/hooks';

import Visitors from './Visitors';
import place from './img/place.svg';
import address from './img/address.svg';
import station from './img/station.svg';
import {
  AddressItem,
  AddressTitle,
  IconWrapper,
  PlaceMap,
  TextInfo,
  Wrapper,
} from './styles';

const ConcertInfo: React.FC = () => {
  const { currentConcert } = useCommonDataStore();

  if (!currentConcert) {
    return null;
  }

  return (
    <Wrapper>
      <div>
        <TextInfo>{currentConcert.description}</TextInfo>
        {currentConcert.place && (
          <>
            <AddressTitle>Адрес</AddressTitle>
            {currentConcert.place.location && (
              <AddressItem highlighted>
                <IconWrapper src={place} />
                <span>{currentConcert.place.location}</span>
              </AddressItem>
            )}
            {currentConcert.place.address && (
              <AddressItem>
                <IconWrapper src={address} />
                <span>{currentConcert.place.address}</span>
              </AddressItem>
            )}
            {currentConcert.place.subway && (
              <AddressItem>
                <IconWrapper src={station} />
                <span>Метро &laquo;{currentConcert.place.subway}&raquo;</span>
              </AddressItem>
            )}
            <PlaceMap>
              <YMaps>
                <Map
                  height="100%"
                  width="100%"
                  state={{
                    center: [
                      currentConcert.place.latitude,
                      currentConcert.place.longitude,
                    ],
                    zoom: 14,
                  }}
                >
                  <Placemark
                    geometry={[
                      currentConcert.place.latitude,
                      currentConcert.place.longitude,
                    ]}
                  />
                </Map>
              </YMaps>
            </PlaceMap>
          </>
        )}
      </div>
      <Visitors />
    </Wrapper>
  );
};

export default React.memo(ConcertInfo);
