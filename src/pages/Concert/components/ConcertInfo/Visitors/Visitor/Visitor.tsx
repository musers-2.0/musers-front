import * as React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import { Text } from 'components/ui';

const Name = styled(Text)`
  transition: color 0.3s linear;
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  cursor: pointer;

  &:hover {
    ${Name} {
      color: white;
    }
  }
`;

const VisitorAvatar = styled.div<{ src: string }>`
  width: 8rem;
  height: 8rem;
  border-radius: 50%;
  background: url(${({ src = '' }) => src}) no-repeat center / cover;
  margin-bottom: 0.8rem;
`;

interface Props {
  avatar: string;
  name: string;
  id: number;
}

const Visitor: React.FC<Props> = ({ avatar, name, id }: Props) => {
  return (
    <Link to={`/user/${id}`}>
      <Wrapper>
        <VisitorAvatar src={avatar} />
        <Name>{name}</Name>
      </Wrapper>
    </Link>
  );
};

export default React.memo(Visitor);
