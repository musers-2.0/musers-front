import * as React from 'react';
import { observer } from 'mobx-react';

import { useCommonDataStore } from 'store/hooks';
import { UserType } from 'store/types';

import Visitor from './Visitor';
import { Cat, VisitorsList, Wrapper } from './styles';

const Visitors: React.FC = () => {
  const { currentConcert } = useCommonDataStore();

  if (!currentConcert) {
    return null;
  }

  return (
    <Wrapper>
      {currentConcert.users.length ? (
        <VisitorsList>
          {currentConcert.users.map((u: UserType, i: number) => (
            <Visitor key={i} name={u.firstName} avatar={u.avatar} id={u.id} />
          ))}
        </VisitorsList>
      ) : (
        <>
          <div>
            На концерт пока никто
            <br />
            не идет :(
          </div>
          <Cat />
        </>
      )}
    </Wrapper>
  );
};

export default observer(Visitors);
