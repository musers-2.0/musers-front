import styled from 'styled-components';

import { COLORS } from 'styles/vars';
import cat from 'img/cat.png';

export const Wrapper = styled.div`
  background: ${COLORS.SURFACE};
  width: 32rem;
  min-height: 36rem;
  border-radius: 3.6rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: center;
  font-size: 2rem;
`;

export const VisitorsList = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(3, 1fr);
  row-gap: 1.6rem;
  column-gap: 1.6rem;
  padding: 2.4rem;
`;

export const Cat = styled.div`
  margin-top: 4.2rem;
  width: 10rem;
  height: 10rem;
  background: url(${cat}) no-repeat center / contain;
`;
