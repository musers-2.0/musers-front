import styled, { css } from 'styled-components';

import { COLORS, MARGINS } from 'styles/vars';
import { Text } from 'components/ui';

export const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  padding: 4rem ${MARGINS.SIDE} 0;
  position: relative;
  z-index: 100;
`;

export const TextInfo = styled(Text)`
  margin-bottom: 3.2rem;
  line-height: 2.4rem;
  width: 70rem;
`;

export const AddressTitle = styled.div`
  font-size: 3.2rem;
  font-weight: 400;
  padding-bottom: 0.8rem;
`;

export const AddressItem = styled(Text)<{ highlighted?: boolean }>`
  margin: 1.8rem 0;
  display: flex;
  align-items: center;
  font-size: 2rem;

  ${({ highlighted = false }: { highlighted?: boolean }) =>
    highlighted &&
    css`
      color: ${COLORS.TEXT};

      & > span {
        border-bottom: 2px ${COLORS.ORANGE} solid;
      }
    `}
`;

export const IconWrapper = styled.div<{ src: string }>`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 4.8rem;
  height: 4.8rem;
  border-radius: 50%;
  margin-right: 1.6rem;
  background: ${COLORS.SURFACE};

  &::before {
    content: '';
    width: 2.4rem;
    height: 2.4rem;
    background: url(${(props: { src: string }) => props.src}) no-repeat center /
      contain;
  }
`;

export const PlaceMap = styled.div`
  width: 100%;
  height: 32rem;
  border-radius: 3.2rem;
  overflow: hidden;
  margin: 2.4rem 0 ${MARGINS.BLOCK_END};
`;
