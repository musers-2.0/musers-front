import * as React from 'react';
import { Virtuoso } from 'react-virtuoso';
import { observer } from 'mobx-react';

import ConcertPreview from 'components/ConcertPreview';
import Loader from 'components/Loader';
import { useCommonDataStore, useUserStore } from 'store/hooks';

import GradientSlider from './GradientSlider';
import { ListWrapper } from './styles';

const ConcertsList: React.FC = () => {
  const userStore = useUserStore();
  const commonDataStore = useCommonDataStore();

  React.useEffect(() => {
    userStore.isAuthorized
      ? userStore.fetchRecommendedConcertsList(true)
      : commonDataStore.fetchConcertsList(true);
  }, []);

  const concertsToShow = userStore.isAuthorized
    ? userStore.recommendedConcerts
    : commonDataStore.concerts;

  const isLoading = userStore.isAuthorized
    ? userStore.isLoading
    : commonDataStore.isLoading;

  return (
    <>
      <Loader isFull show={isLoading} />
      {!isLoading && (
        <>
          <GradientSlider />
          <ListWrapper>
            <Virtuoso
              style={{ height: '90rem' }}
              totalCount={concertsToShow.length || 0}
              itemContent={(i: number) => (
                <ConcertPreview hasPaddings key={i} {...concertsToShow[i]} />
              )}
              endReached={
                userStore.isAuthorized
                  ? () => userStore.fetchRecommendedConcertsList()
                  : () => commonDataStore.fetchConcertsList()
              }
            />
          </ListWrapper>
        </>
      )}
    </>
  );
};

export default observer(ConcertsList);
