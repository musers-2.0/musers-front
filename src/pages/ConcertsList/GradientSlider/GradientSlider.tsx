import * as React from 'react';
import Slider from 'react-slick';
import { Link } from 'react-router-dom';
import { observer } from 'mobx-react';

import { Text, Title } from 'components/ui';
import { useCommonDataStore } from 'store/hooks';
import { ConcertType } from 'store/types';
import { getDayAndMonth } from 'utils/date';

import { Concert, ConcertInfo } from './styles';

const GradientSlider: React.FC = () => {
  const { concerts } = useCommonDataStore();

  return (
    <Slider slidesToShow={1} slidesToScroll={1} dots autoplay swipeToSlide>
      {concerts.slice(0, 5).map((c: ConcertType) => (
        <Concert key={c.id} src={c.image}>
          <Link to={`/concert/${c.id}`}>
            <ConcertInfo>
              <Title>{c.title}</Title>
              <Text>
                {c.place?.location}, {getDayAndMonth(c.startDate)}
              </Text>
            </ConcertInfo>
          </Link>
        </Concert>
      ))}
    </Slider>
  );
};

export default observer(GradientSlider);
