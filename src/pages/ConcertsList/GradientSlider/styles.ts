import styled from 'styled-components';

import { MARGINS } from 'styles/vars';
import { bottomGradientOverlay } from 'styles/mixins';

export const Concert = styled.div<{ src: string }>`
  background: url(${({ src }) => src}) no-repeat center / cover;
  height: 70vh;
  ${bottomGradientOverlay};
`;

export const ConcertInfo = styled.div`
  position: absolute;
  left: ${MARGINS.SIDE};
  bottom: 2rem;
  cursor: pointer;
  transition: transform 0.3s linear;

  &:hover {
    transform: scale(1.01);
  }
`;
