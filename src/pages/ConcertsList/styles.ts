import styled from 'styled-components';

export const ListWrapper = styled.div`
  padding: 5rem 0;

  & > div::-webkit-scrollbar {
    display: none;
  }
`;
