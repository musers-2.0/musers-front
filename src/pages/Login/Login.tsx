import * as React from 'react';

import { Title } from 'components/ui';
import { oauthUrl } from 'config/urls';

import Panel from './Panel';
import {
  HalfPart,
  LeftPart,
  LoginButton,
  LoginContent,
  LoginInfo,
  PageWrapper,
  VkIcon,
} from './styles';

const Login: React.FC = () => {
  return (
    <PageWrapper>
      <LeftPart>
        <LoginContent>
          <Title>Авторизация</Title>
          <LoginInfo>
            На нашем сервисе доступна авторизация только через Вконтакте. Для
            того, чтобы зарегистрироваться или войти, пожалуйста, нажмите кнопку
            ниже
          </LoginInfo>
          <a href={oauthUrl}>
            <LoginButton>
              <VkIcon />
              <div>Войти</div>
            </LoginButton>
          </a>
        </LoginContent>
      </LeftPart>
      <HalfPart>
        <Panel />
      </HalfPart>
    </PageWrapper>
  );
};

export default React.memo(Login);
