import * as React from 'react';
import styled from 'styled-components';

import mock from 'img/mock.jpg';

const Wrapper = styled.div`
  min-width: 18rem;
  margin-right: 6rem;
  transform: skew(30deg);

  &:last-child {
    margin: 0;
  }
`;

const Image = styled.div<{ src: string }>`
  height: 18rem;
  border-radius: 2.4rem;
  background: url(${(props) => props.src}) no-repeat center / cover;
  box-shadow: 0 34px 104px #8e3733;
`;

const Name = styled.div`
  margin-top: 1rem;
  font-size: 2.4rem;
  text-align: center;
  font-weight: 300;
`;

interface Props {
  image: string;
  name: string;
}

const Card: React.FC<Props> = ({ image, name }: Props) => {
  return (
    <Wrapper>
      <Image src={image} />
      <Name>{name}</Name>
    </Wrapper>
  );
};

export default React.memo(Card);
