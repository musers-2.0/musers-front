import * as React from 'react';

import loadImages from 'utils/loadImages';
import Loader from 'components/Loader';

import Card from './Card';
import config, { images } from './config';
import { Row, Rows, Wrapper } from './styles';

const Panel: React.FC = () => {
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    loadImages(images).then(() => setIsLoading(false));
  }, []);

  return (
    <Wrapper>
      <Loader show={isLoading} />
      {!isLoading && (
        <Rows>
          {config.map((r, i) => (
            <Row key={i}>
              {r.map(({ image, name }, index) => (
                <Card image={image} name={name} key={index} />
              ))}
            </Row>
          ))}
        </Rows>
      )}
    </Wrapper>
  );
};

export default React.memo(Panel);
