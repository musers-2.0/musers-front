export const images = [
  require('./img/1.jpg'),
  require('./img/2.jpg'),
  require('./img/3.jpg'),
  require('./img/4.jpg'),
  require('./img/5.jpg'),
  require('./img/6.jpg'),
  require('./img/7.jpg'),
  require('./img/8.jpg'),
  require('./img/9.jpg'),
  require('./img/10.jpg'),
];

export default [
  [
    {
      image: images[7],
      name: 'Placebo',
    },
    {
      image: images[2],
      name: 'Dua Lipa',
    },
    {
      image: images[1],
      name: 'Скриптонит',
    },
  ],
  [
    {
      image: images[5],
      name: 'Сплин',
    },
    {
      image: images[4],
      name: 'Billie Eilish',
    },
    {
      image: images[3],
      name: 'Muse',
    },
    {
      image: images[8],
      name: 'Noize MC',
    },
  ],
  [
    {
      image: images[0],
      name: 'Imagine Dragons',
    },
    {
      image: images[9],
      name: 'KoЯn',
    },
    {
      image: images[6],
      name: 'Fall Out Boy',
    },
  ],
];
