import styled from 'styled-components';

export const Wrapper = styled.div`
  height: 100%;
  background: linear-gradient(180deg, #ff835f 0%, #ff7573 100%);
  overflow: hidden;
  position: relative;
`;

export const Rows = styled.div`
  height: 100%;
  transform: rotate(-30deg);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const Row = styled.div`
  display: flex;
  margin-bottom: 6rem;
  transition: transform 0.5s ease-in-out;

  &:nth-child(1) {
    transform: translate(10%);

    &:hover {
      transform: translate(-25%);
    }
  }

  &:nth-child(2) {
    &:hover {
      transform: translate(-15%);
    }
  }

  &:nth-child(3) {
    transform: translate(10%);
    margin: 0;

    &:hover {
      transform: translate(-15%);
    }
  }
`;
