import styled from 'styled-components';

import { Button, Text } from 'components/ui';
import { ANIMATIONS, COLORS } from 'styles/vars';
import Vk from 'img/vk.component.svg';

export const PageWrapper = styled.div`
  height: 100vh;
  display: flex;
`;

export const HalfPart = styled.div`
  display: inline-block;
  width: 50%;
  height: 100%;
`;

export const LeftPart = styled(HalfPart)`
  display: inline-flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const LoginContent = styled.div`
  width: 44rem;
`;

export const LoginInfo = styled(Text)`
  line-height: 2.4rem;
  margin-bottom: 3rem;
`;

export const LoginButton = styled(Button)`
  background: ${COLORS.SURFACE};
  color: ${COLORS.DIM_TEXT};
  ${ANIMATIONS.TEXT_HOVER};
  display: inline-flex;
  align-items: center;
`;

export const VkIcon = styled(Vk)`
  width: 2.8rem;
  height: 1.6rem;
  margin-right: 1rem;
`;
