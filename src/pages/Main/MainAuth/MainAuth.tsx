import * as React from 'react';
import { observer } from 'mobx-react';

import { useUserStore } from 'store/hooks';
import Loader from 'components/Loader';

import Users from '../components/Users';
import Sharing from '../components/Sharing';

import RecommendedConcerts from './components/RecommendedConcerts';
import TopThreeConcerts from './components/TopThreeConcerts';

const MainAuth: React.FC = () => {
  const userStore = useUserStore();

  React.useEffect(() => {
    userStore.fetchMainPageData();
  }, []);

  return (
    <>
      {userStore.isLoading ? (
        <Loader isFull show />
      ) : (
        <>
          <RecommendedConcerts />
          <TopThreeConcerts />
          <Users isRecommended />
          <Sharing blackBackground />
        </>
      )}
    </>
  );
};

export default observer(MainAuth);
