import * as React from 'react';
import { Link } from 'react-router-dom';

import { About, Artist, Card, Info } from './styles';
import { getDayAndMonth } from '../../../../../../utils/date';

interface Props {
  id: number;
  image: string;
  title: string;
  location?: string;
  date: string;
}

const Concert: React.FC<Props> = ({
  image,
  title,
  id,
  location,
  date,
}: Props) => {
  return (
    <Link to={`/concert/${id}`}>
      <Card src={image}>
        <Info>
          <Artist>{title}</Artist>
          <About>
            {location ? `${location}, ` : ''}
            {getDayAndMonth(date)}
          </About>
        </Info>
      </Card>
    </Link>
  );
};

export default React.memo(Concert);
