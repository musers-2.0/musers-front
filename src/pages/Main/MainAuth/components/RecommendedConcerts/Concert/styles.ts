import styled from 'styled-components';

import { gradientOverlay } from 'styles/mixins';
import { Text } from 'components/ui';

export const Card = styled.div<{ src: string }>`
  height: 25rem;
  border-radius: 3.6rem;
  background: url(${({ src = '' }) => src}) no-repeat top / cover;
  margin: 0 1.6rem;
  ${gradientOverlay};
  cursor: pointer;

  &:hover {
    &::before {
      opacity: 0.8;
    }
  }
`;

export const Info = styled.div`
  position: absolute;
  bottom: 2.4rem;
  left: 3.2rem;
`;

export const Artist = styled.div`
  font-weight: 600;
  font-size: 3.4rem;
  line-height: 4rem;
  margin-bottom: 0.8rem;
`;

export const About = styled(Text)`
  font-size: 2rem;
  line-height: 2.4rem;
`;
