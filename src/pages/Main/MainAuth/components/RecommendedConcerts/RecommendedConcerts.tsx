import * as React from 'react';
import { Link } from 'react-router-dom';
import { observer } from 'mobx-react';

import { Title, UnderlinedText } from 'components/ui';
import Carousel from 'components/Carousel';
import { useUserStore } from 'store/hooks';
import { ConcertType } from 'store/types';

import Concert from './Concert';
import { Info, HeadLine, Wrapper } from './styles';

const RecommendedConcerts: React.FC = () => {
  const { recommendedConcerts } = useUserStore();

  return recommendedConcerts.length ? (
    <Wrapper>
      <HeadLine>
        <Title>Рекомендованные концерты</Title>
        <UnderlinedText>
          <Link to="/concerts">Смотреть все</Link>
        </UnderlinedText>
      </HeadLine>
      <Info>Концерты исполнителей из вашего плейлиста ВК</Info>
      <Carousel>
        {recommendedConcerts.slice(-3).map((c: ConcertType) => (
          <Concert
            key={c.id}
            image={c.image}
            title={c.shortTitle}
            id={c.id}
            location={c.place?.location}
            date={c.startDate}
          />
        ))}
      </Carousel>
    </Wrapper>
  ) : null;
};

export default observer(RecommendedConcerts);
