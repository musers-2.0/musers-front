import styled from 'styled-components';

import { Text } from 'components/ui';
import { MARGINS } from 'styles/vars';

export const Wrapper = styled.div`
  padding: ${MARGINS.BLOCK_END} ${MARGINS.SIDE} 4rem;
`;

export const HeadLine = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const Info = styled(Text)`
  margin-bottom: 3rem;
`;
