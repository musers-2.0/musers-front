import * as React from 'react';
import { observer } from 'mobx-react';

import ConcertPreview from 'components/ConcertPreview';
import { useUserStore } from 'store/hooks';
import { ConcertType } from 'store/types';

import { Wrapper } from './styles';

const TopThreeConcerts: React.FC = () => {
  const { recommendedConcerts } = useUserStore();

  return recommendedConcerts.length ? (
    <Wrapper>
      {recommendedConcerts.slice(0, 3).map((c: ConcertType) => (
        <ConcertPreview key={c.id} {...c} />
      ))}
    </Wrapper>
  ) : null;
};

export default observer(TopThreeConcerts);
