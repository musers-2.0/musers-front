import styled from 'styled-components';

import { MARGINS } from 'styles/vars';

export const Wrapper = styled.div`
  margin: 0 ${MARGINS.SIDE} ${MARGINS.BLOCK_END};
`;
