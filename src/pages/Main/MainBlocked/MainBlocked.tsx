import * as React from 'react';
import { observer } from 'mobx-react';

import GradientHeadedBlock from 'components/GradientHeadedBlock';
import { useCommonDataStore, useUserStore } from 'store/hooks';
import Loader from 'components/Loader';

import PopularConcerts from '../components/PopularConcerts';
import Users from '../components/Users';
import Sharing from '../components/Sharing';

import { Warning } from './styles';

const MainBlocked: React.FC = () => {
  const [warningHidden, hideWarning] = React.useState(false);
  const commonDataStore = useCommonDataStore();
  const userStore = useUserStore();

  React.useEffect(() => {
    commonDataStore.fetchMainPageData();
  }, []);

  return (
    <>
      <Loader isFull show={commonDataStore.isLoading} />
      <Warning hide={warningHidden}>
        {userStore.isBlocked && (
          <GradientHeadedBlock
            title="Доступ к аудио заблокирован"
            onClose={() => hideWarning(true)}
          >
            Кажется, доступ к вашим аудиозаписям Вконтакте закрыт!
            <br />
            Из-за этого мы не можем рекомендовать вам классные концерты или
            новые знакомства :(
            <br />
            Чтобы получить рекомендации, пожалуйста, откройте список ваших
            аудиозаписей
          </GradientHeadedBlock>
        )}
        {userStore.isProcessing && (
          <GradientHeadedBlock
            title="Аудиозаписи еще в обработке"
            onClose={() => hideWarning(true)}
          >
            Упс, придется немного подождать! Кажется, ваши аудиозаписи все еще
            обрабатываются.
            <br />
            Пожалуйста, подождите чуть-чуть или попробуйте обновить страницу.
          </GradientHeadedBlock>
        )}
      </Warning>
      <PopularConcerts />
      <Users />
      <Sharing blackBackground />
    </>
  );
};

export default observer(MainBlocked);
