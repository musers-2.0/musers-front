import styled, { css } from 'styled-components';

import { MARGINS } from 'styles/vars';

interface WarningProps {
  hide: boolean;
}

export const Warning = styled.div<WarningProps>`
  margin: ${MARGINS.BLOCK_END} ${MARGINS.SIDE} 0;
  transition: all 0.3s linear;
  overflow: hidden;
  max-height: initial;

  ${({ hide }: WarningProps) =>
    hide &&
    css`
      opacity: 0;
      max-height: 0;
      margin-top: 0;
    `}
`;
