import * as React from 'react';
import { observer } from 'mobx-react';
import { Route } from 'react-router';

import { useUserStore } from 'store/hooks';

import MainAuth from './MainAuth';
import MainUnauth from './MainUnauth';
import MainBlocked from './MainBlocked';

const MainRoute: React.FC = () => {
  const { isAuthorized, hasAccessToAudios } = useUserStore();

  if (isAuthorized && hasAccessToAudios) {
    return <Route path="/" component={MainAuth} />;
  }

  if (isAuthorized && !hasAccessToAudios) {
    return <Route path="/" component={MainBlocked} />;
  }

  return <Route path="/" component={MainUnauth} />;
};

export default observer(MainRoute);
