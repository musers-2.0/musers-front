import * as React from 'react';
import { observer } from 'mobx-react';

import useScrollTo from 'utils/useScrollTo';
import { useCommonDataStore } from 'store/hooks';
import Loader from 'components/Loader';

import PopularConcerts from '../components/PopularConcerts';
import Users from '../components/Users';
import Sharing from '../components/Sharing';

import Promo from './components/Promo';
import About from './components/About';
import Tutorial from './components/Tutorial';

const MainUnauth: React.FC = () => {
  const [ref, scrollTo] = useScrollTo();

  const commonDataStore = useCommonDataStore();

  React.useEffect(() => {
    commonDataStore.fetchMainPageData();
  }, []);

  return (
    <>
      <Loader isFull show={commonDataStore.isLoading} />
      <Promo />
      <About onButtonClick={scrollTo} />
      <PopularConcerts />
      <Users />
      <Tutorial elementRef={ref} />
      <Sharing />
    </>
  );
};

export default observer(MainUnauth);
