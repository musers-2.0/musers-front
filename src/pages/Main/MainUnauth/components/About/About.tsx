import * as React from 'react';

import { BlackButton, Text, Wrapper } from './styles';

interface Props {
  onButtonClick: () => void;
}

const About: React.FC<Props> = ({ onButtonClick }: Props) => {
  return (
    <Wrapper>
      <Text>
        Musers – это умный сервис для поиска не только крутых исполнителей и
        концертов, но и музыкальных единомышленников. Все, что нужно - ваши
        аудиозаписи. Остальное сервис сделаем сам.
      </Text>
      <BlackButton onClick={onButtonClick}>Узнать больше</BlackButton>
    </Wrapper>
  );
};

export default React.memo(About);
