import styled from 'styled-components';

import { COLORS, GRADIENTS, MARGINS } from 'styles/vars';
import { Button } from 'components/ui';

export const Wrapper = styled.div`
  width: 100%;
  padding: 4rem ${MARGINS.SIDE};
  background: ${GRADIENTS.PINK_RADIAL}, ${GRADIENTS.PINK_LINEAR};
  background-size: 160%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Text = styled.div`
  width: 100%;
  font-size: 2.8rem;
  line-height: 3.2rem;
  text-align: center;
  font-weight: 200;
`;

export const BlackButton = styled(Button)`
  background: ${COLORS.BACKGROUND};
  margin-top: 3rem;
  box-shadow: 0px 4px 14px rgba(32, 34, 41, 0.25),
    0px 14px 54px rgba(32, 34, 41, 0.7);
`;
