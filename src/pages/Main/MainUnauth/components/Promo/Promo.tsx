import * as React from 'react';
import { Link } from 'react-router-dom';

import { GradientButton } from 'components/ui';

import config from './config';
import { Album, Albums, Text, Wrapper } from './styles';

const Promo: React.FC = () => {
  return (
    <Wrapper>
      <div>
        <Text>
          Эти концерты вам точно <span>понравятся</span>.
          <br />
          Осталось выбрать.
        </Text>
        <GradientButton>
          <Link to="login">Попробовать</Link>
        </GradientButton>
      </div>
      <Albums>
        {config.map((a: string, i: number) => (
          <Album src={a} key={i} />
        ))}
      </Albums>
    </Wrapper>
  );
};

export default React.memo(Promo);
