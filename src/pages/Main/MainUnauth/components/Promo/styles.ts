import styled from 'styled-components';

import { GRADIENTS, MARGINS } from 'styles/vars';
import { gradientText } from 'styles/mixins';

export const Wrapper = styled.div`
  width: 100%;
  padding: ${MARGINS.BLOCK_END} ${MARGINS.SIDE};
  display: flex;
  align-items: center;
  justify-content: space-between;
  overflow: hidden;
`;

export const Text = styled.div`
  max-width: 50rem;
  font-size: 4.2rem;
  margin-bottom: 4.5rem;

  & > span {
    ${gradientText(GRADIENTS.PINK_RADIAL)};
    background-size: 200% 100%;
  }
`;

export const Albums = styled.div`
  position: relative;
  margin-left: 3rem;
`;

export const Album = styled.div<{ src: string }>`
  display: inline-block;
  width: 24rem;
  height: 24rem;
  border-radius: 2rem;
  background: url(${(props: { src: string }) => props.src}) no-repeat center / contain;
  transition: transform 0.3s linear;
  box-shadow: 0 14px 64px rgba(0, 0, 0, 0.8);
  position: absolute;
  top: 50%;

  &:nth-child(1) {
    transform: translate(0, -50%) perspective(200px) rotate3d(0, -0.1, 0, 3deg);
    z-index: 3;
    right: 32rem;

    &:hover {
      transform: translate(0, -50%) perspective(200px) rotate3d(0, -0.1, 0, 0);
    }
  }

  &:nth-child(2) {
    transform: translate(0, -50%) perspective(200px) rotate3d(0, -0.1, 0, 4deg);
    z-index: 2;
    right: 16rem;

    &:hover {
      transform: translate(0, -50%) perspective(200px)
        rotate3d(0, -0.1, 0, 1deg);
    }
  }

  &:nth-child(3) {
    transform: translate(0, -50%) perspective(200px) rotate3d(0, -0.1, 0, 5deg);
    z-index: 1;
    right: 0;

    &:hover {
      transform: translate(0, -50%) perspective(200px)
        rotate3d(0, -0.1, 0, 2deg);
    }
  }
`;
