import * as React from 'react';

import { Title } from 'components/ui';

import config from './config';
import { Description, Step, StepNumber, Wrapper } from './styles';

interface Props {
  elementRef: React.RefObject<HTMLInputElement>;
}

const Tutorial: React.FC<Props> = ({ elementRef }: Props) => {
  return (
    <Wrapper ref={elementRef}>
      {config.map(({ step, title, description }) => (
        <Step key={step}>
          <StepNumber>{step}</StepNumber>
          <div>
            <Title>{title}</Title>
            <Description>{description}</Description>
          </div>
        </Step>
      ))}
    </Wrapper>
  );
};

export default React.memo(Tutorial);
