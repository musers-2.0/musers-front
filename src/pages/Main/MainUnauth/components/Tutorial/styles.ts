import styled from 'styled-components';

import { COLORS, GRADIENTS, MARGINS } from 'styles/vars';
import { gradientText } from 'styles/mixins';
import { Text } from 'components/ui';

export const Wrapper = styled.div`
  width: 100%;
  background: ${COLORS.BLACK};
  padding: ${MARGINS.BLOCK_END} ${MARGINS.SIDE};
`;

export const Step = styled.div`
  display: flex;
  padding-bottom: 5rem;

  &:last-child {
    padding: 0;
  }
`;

export const StepNumber = styled.div`
  display: inline-block;
  ${gradientText(GRADIENTS.PINK_RADIAL)};
  background-size: 300%;
  font-size: 9.2rem;
  line-height: 9rem;
  font-weight: 900;
  min-width: 12rem;
`;

export const Description = styled(Text)`
  line-height: 3.2rem;
`;
