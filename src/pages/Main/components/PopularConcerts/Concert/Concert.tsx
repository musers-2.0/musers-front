import * as React from 'react';
import { Link } from 'react-router-dom';

import { ConcertType } from 'store/types';
import { getDayMonthAndTime } from 'utils/date';

import { Image, Info, Title, Wrapper } from './styles';

const Concert: React.FC<ConcertType> = ({
  id,
  shortTitle,
  image,
  startDate,
  place,
}: ConcertType) => {
  return (
    <Link to={`/concert/${id}`}>
      <Wrapper>
        <Image src={image} />
        <Title>{shortTitle}</Title>
        <Info>
          {getDayMonthAndTime(startDate)} – {place?.title}
        </Info>
      </Wrapper>
    </Link>
  );
};

export default React.memo(Concert);
