import styled from 'styled-components';

import { Text } from 'components/ui';
import { gradientOverlay } from 'styles/mixins';

export const Image = styled.div<{ src?: string }>`
  width: 100%;
  height: 25rem;
  border-radius: 2.4rem;
  background: url(${(props) => props.src || ''}) no-repeat top / cover;
  margin-bottom: 2rem;
  ${gradientOverlay}
`;

export const Wrapper = styled.div`
  cursor: pointer;
  margin: 0 1.6rem;

  &:hover {
    ${Image} {
      &::before {
        opacity: 0.8;
      }
    }
  }
`;

export const Title = styled.div`
  font-size: 3rem;
  margin-bottom: 1rem;
  max-width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const Info = styled(Text)`
  max-width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
`;
