import * as React from 'react';
import { observer } from 'mobx-react';
import { Link } from 'react-router-dom';

import { Text, Title } from 'components/ui';
import Carousel from 'components/Carousel';
import { useCommonDataStore } from 'store/hooks';
import { ConcertType } from 'store/types';

import Concert from './Concert';
import { Concerts, ShowAll, Wrapper } from './styles';

const PopularConcerts: React.FC = () => {
  const { concerts } = useCommonDataStore();

  return concerts.length ? (
    <Wrapper>
      <Title>Популярные концерты</Title>
      <Text>Концерты исполнителей, популярных у пользователей сервиса</Text>
      <ShowAll>
        <Link to="/concerts">Смотреть все</Link>
      </ShowAll>
      <Concerts>
        <Carousel>
          {concerts.map((c: ConcertType) => (
            <Concert key={c.id} {...c} />
          ))}
        </Carousel>
      </Concerts>
    </Wrapper>
  ) : null;
};

export default observer(PopularConcerts);
