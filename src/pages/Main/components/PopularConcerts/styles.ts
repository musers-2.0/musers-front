import styled from 'styled-components';

import { MARGINS } from 'styles/vars';
import { UnderlinedText } from 'components/ui';

export const Concerts = styled.div`
  margin-top: 2.5rem;
`;

export const Wrapper = styled.div`
  width: 100%;
  padding: ${MARGINS.BLOCK_END} ${MARGINS.SIDE};
  position: relative;
`;

export const ShowAll = styled(UnderlinedText)`
  position: absolute;
  top: 7rem;
  right: 10rem;
  line-height: 4.2rem;
`;
