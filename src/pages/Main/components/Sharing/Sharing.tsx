import * as React from 'react';

import { GradientButton } from 'components/ui';

import { SharingProps } from './types';
import { SharingLogo, SharingText, Text, Wrapper } from './styles';

const Sharing: React.FC<SharingProps> = ({ blackBackground }: SharingProps) => {
  return (
    <Wrapper blackBackground={blackBackground}>
      <Text>
        Нравится наш сервис? Расскажите о нем друзьям и ходите на концерты
        вместе!
      </Text>
      <GradientButton>
        <SharingText>
          <SharingLogo />
          <div>Поделиться</div>
        </SharingText>
      </GradientButton>
    </Wrapper>
  );
};

Sharing.defaultProps = {
  blackBackground: false,
};

export default React.memo(Sharing);
