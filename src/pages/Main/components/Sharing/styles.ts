import styled, { css } from 'styled-components';

import { COLORS, MARGINS } from 'styles/vars';
import VkIcon from 'img/vk.component.svg';

import { SharingProps } from './types';

export const Wrapper = styled.div<SharingProps>`
  padding: 4rem ${MARGINS.SIDE};
  display: flex;
  flex-direction: column;
  align-items: center;

  ${(props: SharingProps) =>
    props.blackBackground &&
    css`
      background: ${COLORS.BLACK};
    `}
`;

export const Text = styled.div`
  max-width: 90rem;
  font-size: 2.8rem;
  line-height: 3.2rem;
  text-align: center;
  font-weight: 200;
  margin-bottom: 3rem;
`;

export const SharingText = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const SharingLogo = styled(VkIcon)`
  width: 2.6rem;
  height: 1.6rem;
  margin-right: 1rem;
`;
