import * as React from 'react';
import { useHistory } from 'react-router-dom';

import { UserType } from 'store/types';

import { Image, Name, Wrapper } from './styles';

type Props = UserType & {
  clickable: boolean;
};

const User: React.FC<Props> = ({
  id,
  firstName,
  avatar,
  clickable,
  age,
}: Props) => {
  const history = useHistory();

  const onClick = React.useCallback(() => {
    if (clickable) {
      return history.push(`/user/${id}`);
    }
  }, [clickable]);

  return (
    <Wrapper onClick={onClick}>
      <Image src={avatar} />
      <Name>
        {firstName}
        {age && <>, {age}</>}
      </Name>
    </Wrapper>
  );
};

export default React.memo(User);
