import styled from 'styled-components';

import { Text } from 'components/ui';
import mock from 'img/mock.jpg';

export const Wrapper = styled.div`
  width: 18rem;
  margin: 1.6rem;
  cursor: pointer;
  flex-shrink: 0;

  &:first-child {
    margin-left: 0;
  }
`;

export const Image = styled.div<{ src: string }>`
  width: 100%;
  height: 18rem;
  border-radius: 3.6rem;
  background: url(${(props) => props.src || mock}) no-repeat center / cover;
  margin-bottom: 2rem;
`;

export const Name = styled(Text)`
  max-width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
`;
