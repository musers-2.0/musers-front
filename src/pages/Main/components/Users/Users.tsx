import * as React from 'react';
import { observer } from 'mobx-react';
import { Link } from 'react-router-dom';

import { Text, Title, UnderlinedText } from 'components/ui';
import { useCommonDataStore, useUserStore } from 'store/hooks';
import { UserType } from 'store/types';
import pluralize from 'utils/pluralize';

import User from './User';
import { HeadLine, UsersList, Wrapper } from './styles';

interface Props {
  isRecommended?: boolean;
}

const Users: React.FC<Props> = ({ isRecommended = false }: Props) => {
  const { users, usersTotal } = useCommonDataStore();
  const {
    isAuthorized,
    recommendedUsers,
    recommendedUsersCount,
  } = useUserStore();

  const canShowRecommendations = isAuthorized && isRecommended;
  const usersToShow = isRecommended ? recommendedUsers : users;

  return usersToShow.length ? (
    <Wrapper>
      <HeadLine>
        <Title>
          {canShowRecommendations
            ? 'Не знаете, с кем пойти?'
            : 'Подберите себе компанию'}
        </Title>
        {canShowRecommendations && (
          <UnderlinedText>
            <Link to="/users">Смотреть все</Link>
          </UnderlinedText>
        )}
      </HeadLine>
      <Text>
        {canShowRecommendations ? (
          <>
            Мы подобрали для вас{' '}
            {pluralize(recommendedUsersCount, [
              'пользователей',
              'пользователя',
              'пользователя',
            ])}
          </>
        ) : (
          <>
            {pluralize(usersTotal, [
              'человек уже пользуются',
              'человек уже пользуется',
              'человека уже пользуются',
            ])}{' '}
            сервисом
          </>
        )}
      </Text>
      <UsersList>
        {usersToShow.map((u: UserType) => (
          <User key={u.id} clickable={isAuthorized} {...u} />
        ))}
      </UsersList>
    </Wrapper>
  ) : null;
};

export default observer(Users);
