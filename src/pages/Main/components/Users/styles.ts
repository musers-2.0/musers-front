import styled from 'styled-components';

import { MARGINS } from 'styles/vars';
import { scrollbarMixin } from 'styles/mixins';

export const Wrapper = styled.div`
  width: 100%;
  padding: ${MARGINS.BLOCK_END} ${MARGINS.SIDE};
`;

export const UsersList = styled.div`
  margin-top: 2.4rem;
  width: 100%;
  display: flex;
  flex-wrap: nowrap;
  overflow: auto;
  ${scrollbarMixin}
`;

export const HeadLine = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
