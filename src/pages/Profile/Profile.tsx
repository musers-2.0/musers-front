import * as React from 'react';
import { observer } from 'mobx-react';

import ProfileHead from './components/ProfileHead';
import tabsConfig, { TabsEnum } from './tabsConfig';
import {
  DataContainer,
  ProfileContent,
  Switch,
  Tab,
  TabInfo,
  Tabs,
} from './styles';

const Profile: React.FC = () => {
  const [tab, setTab] = React.useState(TabsEnum.concerts);
  const { info, Component } = tabsConfig[tab];

  const tabs = React.useMemo(() => Object.values(tabsConfig), []);

  return (
    <>
      <ProfileHead />
      <ProfileContent>
        <Switch>
          <Tabs>
            {tabs.map((t) => (
              <Tab
                active={tab === t.type}
                key={t.type}
                onClick={() => setTab(t.type)}
              >
                {t.name}
              </Tab>
            ))}
          </Tabs>
          <TabInfo>{info}</TabInfo>
        </Switch>
        <Component />
      </ProfileContent>
    </>
  );
};

export default observer(Profile);
