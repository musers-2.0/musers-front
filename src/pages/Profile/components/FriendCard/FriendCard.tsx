import * as React from 'react';

import {
  ConcertsCounter,
  FriendCardWrapper,
  Image,
  Info,
  RockLogo,
  Title,
} from './styles';

const FriendCard: React.FC = () => {
  return (
    <FriendCardWrapper>
      <Image />
      <div>
        <Title>Надежда Меркулова</Title>
        <Info>Москва, 16 лет</Info>
      </div>
      <ConcertsCounter>
        <RockLogo />
        <div>5</div>
      </ConcertsCounter>
    </FriendCardWrapper>
  );
};

export default React.memo(FriendCard);
