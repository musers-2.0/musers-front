import styled from 'styled-components';

import { COLORS } from 'styles/vars';
import mock from 'img/mock.jpg';
import Rock from 'img/rock.component.svg';

export const FriendCardWrapper = styled.div`
  height: 12rem;
  position: relative;
  display: flex;
  align-items: center;
  cursor: pointer;

  &::before {
    content: '';
    z-index: -1;
    position: absolute;
    top: 0;
    left: 0.4rem;
    right: 0;
    height: 100%;
    background: ${COLORS.SURFACE};
    border-radius: 2.4rem;
  }
`;

export const Image = styled.div`
  width: 8.5rem;
  height: 8.5rem;
  border-radius: 2.4rem;
  margin-right: 2.5rem;
  background: url(${mock}) no-repeat center / cover;
`;

export const Title = styled.div`
  font-size: 2.4rem;
  line-height: 3.6rem;
  margin-bottom: 1.2rem;
`;

export const Info = styled.div`
  color: ${COLORS.DIM_TEXT};
  font-weight: 300;
  font-size: 1.6rem;
  line-height: 2rem;
`;

export const ConcertsCounter = styled.div`
  position: absolute;
  top: 1.2rem;
  right: 1.2rem;
  display: flex;
  align-items: center;
  border-radius: 2.4rem;
  background: ${COLORS.BACKGROUND};
  padding: 1rem 1.6rem;
  font-size: 1.6rem;
  line-height: 2.4rem;
  color: ${COLORS.DIM_TEXT};
`;

export const RockLogo = styled(Rock)`
  width: 1.6rem;
  height: 1.6rem;
  color: ${COLORS.ORANGE};
  margin-right: 0.8rem;
`;
