import * as React from 'react';
import { observer } from 'mobx-react';

import { useUserStore } from 'store/hooks';
import pluralize from 'utils/pluralize';

import { Avatar, Header, Info, Name, UserLink } from './styles';

const ProfileHead: React.FC = () => {
  const { currentUser } = useUserStore();

  return (
    <Header>
      <Avatar src={currentUser.avatar} />
      <div>
        <Name>
          {currentUser.firstName} {currentUser.lastName}
        </Name>
        {currentUser.domain && (
          <UserLink>
            <a href={`https://vk.com/${currentUser.domain}`} target="_blank">
              {currentUser.domain}
            </a>
            ,
          </UserLink>
        )}
        {currentUser.age && (
          <Info>{pluralize(currentUser.age, ['лет', 'год', 'года'])}</Info>
        )}
      </div>
    </Header>
  );
};

export default observer(ProfileHead);
