import styled from 'styled-components';

import { GRADIENTS, MARGINS } from 'styles/vars';

export const Header = styled.div`
  background: ${GRADIENTS.ORANGE_RADIAL};
  border-radius: 0 0 3rem 3rem;
  padding: 6rem ${MARGINS.SIDE};
  display: flex;
`;

export const Avatar = styled.div<{ src: string }>`
  width: 10rem;
  height: 10rem;
  background: url(${({ src = '' }) => src}) no-repeat center / cover;
  border-radius: 2.8rem;
  margin-right: 2.2rem;
  align-self: center;
`;

export const Name = styled.div`
  font-weight: 600;
  font-size: 4rem;
  line-height: 5.8rem;
`;

export const Info = styled.div`
  font-size: 2rem;
  line-height: 3rem;
  display: inline-block;
`;

export const UserLink = styled(Info)`
  text-decoration: underline;
  margin-right: 0.5rem;
`;
