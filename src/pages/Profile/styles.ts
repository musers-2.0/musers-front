import styled from 'styled-components';

import { COLORS, MARGINS } from 'styles/vars';
import cat from 'img/cat.png';

export const ProfileContent = styled.div`
  padding: 0 ${MARGINS.SIDE};
  min-height: 65vh;
  position: relative;
`;

export const Switch = styled.div`
  padding: 3.2rem 0;
`;

export const Tabs = styled.div`
  display: flex;
  font-size: 2.4rem;
  line-height: 2.8rem;
  font-weight: 300;
`;

export const Tab = styled.div<{ active?: boolean }>`
  margin-right: 2rem;
  transition: opacity 0.3s linear;
  cursor: pointer;
  opacity: ${({ active }) => (active ? 1 : 0.3)};

  &:last-child {
    margin-right: 0;
  }

  &:hover {
    opacity: 1;
  }
`;

export const TabInfo = styled.div`
  margin-top: 2rem;
  font-size: 1.8rem;
  line-height: 2.4rem;
  opacity: 0.3;
`;

export const DataContainer = styled.div`
  min-height: 60vh;
  max-height: 90vh;
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  flex-wrap: wrap;
  overflow: auto;
  position: relative;
`;

export const CardWrapper = styled.div`
  width: calc(50% - 3.2rem);
`;

export const Mock = styled.div`
  font-size: 3rem;
  line-height: 3.2rem;
  text-align: center;
  margin: auto;
  color: ${COLORS.DIM_TEXT};
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -200%);

  &::after {
    content: '';
    display: block;
    width: 13rem;
    height: 15rem;
    background: url(${cat}) no-repeat top / contain;
    position: absolute;
    bottom: -4rem;
    left: 50%;
    transform: translate(-50%, 100%);
  }
`;
