import * as React from 'react';
import { observer } from 'mobx-react';

import { useUserStore } from 'store/hooks';
import { ConcertsByDateEnum, ShortConcertType } from 'store/types';
import ConcertCard from 'components/ConcertCard';
import Loader from 'components/Loader';

import { CardWrapper, DataContainer, Mock } from '../../styles';

const ConcertsTab: React.FC = () => {
  const userStore = useUserStore();

  React.useEffect(() => {
    userStore.fetchUserConcerts(ConcertsByDateEnum.coming);
  }, []);

  return (
    <>
      <Loader show={userStore.isLoading} />
      {!userStore.isLoading && (
        <>
          {userStore.usersComingConcertsCount === 0 ? (
            <Mock>
              Вы еще не&nbsp;собираетесь
              <br />
              ни&nbsp;на&nbsp;один концерт :(
            </Mock>
          ) : (
            <DataContainer>
              {userStore.usersComingConcerts.map((c: ShortConcertType) => (
                <CardWrapper key={c.id}>
                  <ConcertCard {...c} />
                </CardWrapper>
              ))}
            </DataContainer>
          )}
        </>
      )}
    </>
  );
};

export default observer(ConcertsTab);
