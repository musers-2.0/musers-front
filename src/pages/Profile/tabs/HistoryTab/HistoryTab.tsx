import * as React from 'react';
import { observer } from 'mobx-react';

import { useUserStore } from 'store/hooks';
import { ConcertsByDateEnum, ShortConcertType } from 'store/types';
import ConcertCard from 'components/ConcertCard';
import Loader from 'components/Loader';

import { CardWrapper, DataContainer, Mock } from '../../styles';

const HistoryTab: React.FC = () => {
  const userStore = useUserStore();

  React.useEffect(() => {
    userStore.fetchUserConcerts(ConcertsByDateEnum.passed);
  }, []);

  return (
    <>
      <Loader show={userStore.isLoading} />
      {!userStore.isLoading && (
        <>
          {userStore.usersPassedConcertsCount === 0 ? (
            <Mock>
              Вы еще не&nbsp;посетили
              <br />
              ни&nbsp;один концерт :(
            </Mock>
          ) : (
            <DataContainer>
              {userStore.usersPassedConcerts.map((c: ShortConcertType) => (
                <CardWrapper key={c.id}>
                  <ConcertCard {...c} />
                </CardWrapper>
              ))}
            </DataContainer>
          )}
        </>
      )}
    </>
  );
};

export default observer(HistoryTab);
