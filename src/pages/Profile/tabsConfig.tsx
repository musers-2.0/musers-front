import React from 'react';

import ConcertsTab from './tabs/ConcertsTab';
import HistoryTab from './tabs/HistoryTab';
// import FriendsTab from './tabs/FriendsTab';

export enum TabsEnum {
  concerts = 'concerts',
  history = 'history',
  // friends = 'friends',
}

export interface TabType {
  name: string;
  info: string;
  type: TabsEnum;
  Component: React.FC;
}

const tabs: Record<TabsEnum, TabType> = {
  [TabsEnum.concerts]: {
    name: 'Концерты',
    info: 'Концерты, на которых вы поставили отметку "Я пойду"',
    type: TabsEnum.concerts,
    Component: ConcertsTab,
  },
  [TabsEnum.history]: {
    name: 'История',
    info: 'Концерты, которые вы посетили',
    type: TabsEnum.history,
    Component: HistoryTab,
  },
  // [TabsEnum.friends]: {
  //   name: 'Друзья',
  //   info: 'Ваши друзья из Вконтакте, которые уже пользуются сервисом',
  //   type: TabsEnum.friends,
  //   Component: FriendsTab,
  // },
};

export default tabs;
