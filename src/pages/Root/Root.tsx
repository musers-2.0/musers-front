import * as React from 'react';
import { Switch, Route } from 'react-router';
import { observer } from 'mobx-react';

import MainRoute from 'pages/Main';
import Login from 'pages/Login';
import ConcertsList from 'pages/ConcertsList';
import Concert from 'pages/Concert';
import User from 'pages/User';
import Profile from 'pages/Profile';
import UsersList from 'pages/UsersList';
import Header from 'components/Header';
import Footer from 'components/Footer';
import Loader from 'components/Loader';
import { useUserStore } from 'store/hooks';
import useScrollTopRouter from 'utils/useScrollTopRouter';

const Root: React.FC = () => {
  const user = useUserStore();

  useScrollTopRouter();

  React.useEffect(() => {
    user.fetchCurrentUser();
  }, []);

  return (
    <>
      {user.isAuthCheckLoading ? (
        <Loader isFull show />
      ) : (
        <>
          <Header />
          <Switch>
            {!user.isAuthorized && <Route path="/login" component={Login} />}
            {user.isAuthorized && <Route path="/profile" component={Profile} />}
            {user.isAuthorized && <Route path="/users" component={UsersList} />}
            <Route path="/concerts" component={ConcertsList} />
            <Route exact path="/concert/:id" component={Concert} />
            {user.isAuthorized && (
              <Route exact path="/user/:id" component={User} />
            )}
            <MainRoute />
          </Switch>
          <Footer />
        </>
      )}
    </>
  );
};

export default observer(Root);
