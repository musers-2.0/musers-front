import { makeAutoObservable, runInAction } from 'mobx';

import {
  ApiConcertType,
  ApiListResponseType,
  ApiUserType,
  ConcertsByDateEnum,
  ShortConcertType,
  UserType,
} from 'store/types';
import api, { ApiResponse } from 'utils/api';
import urls from 'config/urls';
import { normalizeConcert, normalizeUser } from 'utils/normalizers';

export default class SelectedUserStore {
  user: UserType;
  userId: number;

  userComingConcerts: ShortConcertType[] = [];
  userComingConcertsCount: number | null = null;

  userPassedConcerts: ShortConcertType[] = [];
  userPassedConcertsCount: number | null = null;

  isLoading = true;
  isConcertsLoading = true;

  constructor(userId: number) {
    makeAutoObservable(this);

    this.userId = userId;
    this.fetchUser();
    this.fetchUserComingConcerts();
  }

  setLoading(value: boolean): void {
    this.isLoading = value;
  }

  setConcertsLoading(value: boolean): void {
    this.isConcertsLoading = value;
  }

  async fetchUser(): Promise<void> {
    this.setLoading(true);

    const { response }: ApiResponse<ApiUserType> = await api(
      urls.userGet,
      'GET',
      { id: this.userId }
    );

    if (response) {
      runInAction(() => {
        this.user = normalizeUser(response);
      });
    }

    this.setLoading(false);
  }

  fetchUserComingConcerts = async (): Promise<void> => {
    this.setConcertsLoading(true);

    const { response }: ApiListResponseType<ApiConcertType> = await api(
      urls.concertList,
      'GET',
      {
        user_id: this.userId,
        date_filter: ConcertsByDateEnum.coming,
      }
    );

    if (response) {
      runInAction(() => {
        this.userComingConcerts = response.items?.map(normalizeConcert) || [];
        this.userComingConcertsCount = response.count;
      });
    }

    this.setConcertsLoading(false);
  };

  fetchUserPassedConcerts = async (): Promise<void> => {
    this.setConcertsLoading(true);

    const { response }: ApiListResponseType<ApiConcertType> = await api(
      urls.concertList,
      'GET',
      {
        user_id: this.userId,
        date_filter: ConcertsByDateEnum.passed,
      }
    );

    if (response) {
      runInAction(() => {
        this.userPassedConcerts = response.items?.map(normalizeConcert) || [];
        this.userPassedConcertsCount = response.count;
      });
    }

    this.setConcertsLoading(false);
  };
}
