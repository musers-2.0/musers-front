import * as React from 'react';
import { useParams } from 'react-router';
import { observer } from 'mobx-react';

import Loader from 'components/Loader';

import LeftPanel from './components/LeftPanel';
import RightBlock from './components/RightBlock';
import SelectedUserStore from './SelectedUserStore';
import { PageWrapper } from './styles';

const User: React.FC = () => {
  const { id } = useParams<{ id: string }>();

  const [
    selectedUser,
    setSelectedUser,
  ] = React.useState<SelectedUserStore | null>(null);

  React.useEffect(() => {
    setSelectedUser(new SelectedUserStore(Number(id)));
  }, []);

  return (
    <PageWrapper>
      {!selectedUser || selectedUser?.isLoading ? (
        <Loader show />
      ) : (
        <>
          <LeftPanel user={selectedUser.user} />
          <RightBlock
            concertsList={selectedUser.userComingConcerts}
            concertsCount={selectedUser.userComingConcertsCount || 0}
            historyList={selectedUser.userPassedConcerts}
            historyCount={selectedUser.userPassedConcertsCount || 0}
            onConcertsTab={selectedUser.fetchUserComingConcerts}
            onHistoryTab={selectedUser.fetchUserPassedConcerts}
          />
        </>
      )}
    </PageWrapper>
  );
};

export default observer(User);
