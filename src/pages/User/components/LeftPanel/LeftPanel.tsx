import * as React from 'react';
import { useHistory } from 'react-router';

import UserCard from 'components/UserCard';
import { UserType } from 'store/types';

import { Arrow, ArrowWrapper, BackButton, Text, Wrapper } from './styles';

interface Props {
  user: UserType;
}

const LeftPanel: React.FC<Props> = ({ user }: Props) => {
  const history = useHistory();

  const onGoBack = React.useCallback(() => {
    history.goBack();
  }, []);

  return (
    <Wrapper>
      <BackButton onClick={onGoBack}>
        <ArrowWrapper>
          <Arrow />
        </ArrowWrapper>
        <Text>Назад</Text>
      </BackButton>
      <UserCard {...user} />
    </Wrapper>
  );
};

export default React.memo(LeftPanel);
