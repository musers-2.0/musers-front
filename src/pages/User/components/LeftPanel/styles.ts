import styled, { keyframes } from 'styled-components';

import { COLORS, GRADIENTS } from 'styles/vars';
import ArrowIcon from 'img/arrow.component.svg';

export const Wrapper = styled.div`
  width: 40%;
  height: 100%;
  background: ${GRADIENTS.ORANGE_RADIAL};
  border-radius: 0 2.4rem 2.4rem 0;
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
`;

export const ArrowWrapper = styled.div`
  display: flex;
  width: 2.4rem;
  height: 2.4rem;
  border-radius: 50%;
  align-items: center;
  justify-content: center;
  background: ${COLORS.SURFACE};
  margin-right: 1rem;
`;

const jump = keyframes`
  0%, 50%, 100% {
  transform: translate(0, 0);
  }
  
  30%, 70% {
    transform: translate(-10%, 0);
  }
`;

export const BackButton = styled.div`
  position: absolute;
  top: 2rem;
  left: 2rem;
  display: flex;
  align-items: center;
  opacity: 0.3;
  cursor: pointer;

  &:hover ${ArrowWrapper} {
    animation: ${jump} 0.5s linear;
  }
`;

export const Arrow = styled(ArrowIcon)`
  color: ${COLORS.DIM_TEXT};
  width: 0.8rem;
  height: 1.8rem;
`;

export const Text = styled.div`
  color: ${COLORS.SURFACE};
  font-size: 1.6rem;
`;
