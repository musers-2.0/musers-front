import * as React from 'react';

import ConcertCard from 'components/ConcertCard';
import { ShortConcertType } from 'store/types';

import { ConcertsContainer, MockWrapper, Switch, Tab, Wrapper } from './styles';
import { Tabs } from './tabsConfig';

interface Props {
  concertsList: ShortConcertType[];
  historyList: ShortConcertType[];
  concertsCount: number;
  historyCount: number;
  onConcertsTab: VoidFunction;
  onHistoryTab: VoidFunction;
}

const RightBlock: React.FC<Props> = ({
  concertsList = [],
  historyList = [],
  concertsCount = 0,
  historyCount = 0,
  onConcertsTab = () => null,
  onHistoryTab = () => null,
}: Props) => {
  const [tab, setTab] = React.useState(Tabs.concerts);

  const handleConcertsTab = React.useCallback(() => {
    if (tab !== Tabs.concerts) {
      setTab(Tabs.concerts);
      onConcertsTab();
    }
  }, [tab, onConcertsTab]);

  const handleHistoryTab = React.useCallback(() => {
    if (tab !== Tabs.history) {
      setTab(Tabs.history);
      onHistoryTab();
    }
  }, [tab, onHistoryTab]);

  return (
    <Wrapper>
      <Switch>
        <Tab active={tab === Tabs.concerts} onClick={handleConcertsTab}>
          Концерты
        </Tab>
        <Tab active={tab === Tabs.history} onClick={handleHistoryTab}>
          История
        </Tab>
      </Switch>
      {tab === Tabs.concerts && (
        <>
          {concertsCount === 0 ? (
            <MockWrapper>
              Этот пользователь пока
              <br />
              не собирается ни на один концерт :(
            </MockWrapper>
          ) : (
            <ConcertsContainer>
              {concertsList.map((c: ShortConcertType) => (
                <ConcertCard {...c} key={c.id} />
              ))}
            </ConcertsContainer>
          )}
        </>
      )}
      {tab === Tabs.history && (
        <>
          {historyCount === 0 ? (
            <MockWrapper>
              Этот пользователь пока
              <br />
              не посетил ни один концерт :(
            </MockWrapper>
          ) : (
            <ConcertsContainer>
              {historyList.map((c: ShortConcertType) => (
                <ConcertCard {...c} key={c.id} />
              ))}
            </ConcertsContainer>
          )}
        </>
      )}
    </Wrapper>
  );
};

export default RightBlock;
