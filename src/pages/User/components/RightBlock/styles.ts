import styled from 'styled-components';

import { COLORS, MARGINS } from 'styles/vars';
import cat from 'img/cat.png';

export const Wrapper = styled.div`
  width: 60%;
  overflow: hidden;
  padding: 6rem ${MARGINS.SIDE} 0;
`;

export const Switch = styled.div`
  margin-bottom: 3.2rem;
  font-size: 2.4rem;
`;

export const Tab = styled.div<{ active?: boolean }>`
  display: inline-block;
  opacity: ${({ active = false }) => (active ? 1 : 0.3)};
  transition: opacity 0.3s linear;
  cursor: pointer;

  &:hover {
    opacity: 0.8;
  }

  &:first-child {
    margin-right: 2rem;
  }
`;

export const ConcertsContainer = styled.div`
  height: calc(100% - 6rem);
  overflow: auto;
`;

export const MockWrapper = styled.div`
  padding: 3.6rem;
  background: ${COLORS.SURFACE};
  border-radius: 3.6rem;
  height: calc(100% - 11rem);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: center;
  font-size: 2rem;
  line-height: 2.4rem;
  font-weight: 300;

  &::after {
    content: '';
    background: url(${cat}) no-repeat center / contain;
    width: 13rem;
    height: 15.5rem;
    margin-top: 4.4rem;
  }
`;
