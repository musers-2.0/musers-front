import styled from 'styled-components';

import { COLORS, GRADIENTS } from 'styles/vars';
import ArrowIcon from 'img/arrow.component.svg';

export const PageWrapper = styled.div`
  height: calc(100vh - 8rem);
  overflow: hidden;
  display: flex;
`;