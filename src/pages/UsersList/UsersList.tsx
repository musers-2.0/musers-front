import * as React from 'react';
import { observer } from 'mobx-react';

import Loader from 'components/Loader';
import UserPreview from 'components/UserPreview';
import { useUserStore } from 'store/hooks';

import { PageWrapper, UserPreviewWrapper, UsersWrapper } from './styles';

const UsersList: React.FC = () => {
  const userStore = useUserStore();

  React.useEffect(() => {
    userStore.fetchRecommendedUsers(true, 20, 0);
  }, []);

  return (
    <PageWrapper>
      <Loader isFull show={userStore.isLoading} />
      {!userStore.isLoading && (
        <UsersWrapper>
          {userStore.recommendedUsers.map((u) => (
            <UserPreviewWrapper key={u.id}>
              <UserPreview {...u} />
            </UserPreviewWrapper>
          ))}
        </UsersWrapper>
      )}
    </PageWrapper>
  );
};

export default observer(UsersList);
