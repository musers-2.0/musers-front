import styled from 'styled-components';

import { GRADIENTS, MARGINS } from 'styles/vars';

export const PageWrapper = styled.div`
  height: calc(100vh - 8rem);
  background: ${GRADIENTS.ORANGE_RADIAL};
  position: relative;
  overflow: auto;
  padding: 4rem ${MARGINS.SIDE};

  &::-webkit-scrollbar {
    display: none;
  }
`;

export const UsersWrapper = styled.div`
  margin: auto;
  display: inline-flex;
  flex-wrap: wrap;
  position: absolute;
  top: 4rem;
  left: 50%;
  transform: translate(-50%, 0);
  min-width: 112rem;

  &::-webkit-scrollbar {
    display: none;
  }
`;

export const UserPreviewWrapper = styled.div`
  margin: 2rem;
`;
