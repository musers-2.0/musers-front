import { makeAutoObservable, runInAction } from 'mobx';

import api, { ApiResponse } from 'utils/api';
import urls from 'config/urls';
import { normalizeConcert, normalizeUser } from 'utils/normalizers';

import { RootStore } from './RootStore';
import {
  ApiConcertType,
  ApiListResponseType,
  ApiUserType,
  ConcertType,
  UserType,
} from './types';

const DEFAULT_LIMIT = 10;

const MAIN_PAGE_CONCERTS_LIMIT = 6;
const MAIN_PAGE_USERS_LIMIT = 15;

const CONCERTS_LIST_LIMIT = 20;

export class CommonDataStore {
  rootStore: RootStore;

  concerts: ConcertType[] = [];
  concertsTotal = 0;

  currentConcert: ConcertType | null = null;

  users: UserType[] = [];
  usersTotal = 0;

  currentConcertsOffset = 0;

  concertsSearchResult: ConcertType[] = [];

  isLoading = true;
  isSearching = false;

  constructor(rootStore: RootStore) {
    makeAutoObservable(this, {
      rootStore: false,
    });

    this.rootStore = rootStore;
  }

  setLoading(value: boolean): void {
    this.isLoading = value;
  }

  async fetchConcerts(offset?: number, limit?: number): Promise<void> {
    const { response }: ApiListResponseType<ApiConcertType> = await api(
      urls.concertList,
      'GET',
      {
        offset: offset || 0,
        limit: limit || DEFAULT_LIMIT,
      }
    );

    if (response) {
      runInAction(() => {
        this.concerts = response.items?.map(normalizeConcert) || [];
        this.concertsTotal = response.count;
      });
    }
  }

  async fetchUsers(offset?: number, limit?: number): Promise<void> {
    const { response }: ApiListResponseType<ApiUserType> = await api(
      urls.userList,
      'GET',
      {
        offset: offset || 0,
        limit: limit || DEFAULT_LIMIT,
      }
    );

    if (response) {
      runInAction(() => {
        this.users = response.items?.map(normalizeUser) || [];
        this.usersTotal = response.count;
      });
    }
  }

  async fetchMainPageData(): Promise<void> {
    this.setLoading(true);

    await Promise.all([
      this.fetchConcerts(0, MAIN_PAGE_CONCERTS_LIMIT),
      this.fetchUsers(0, MAIN_PAGE_USERS_LIMIT),
    ]);

    this.setLoading(false);
  }

  async fetchConcert(id: string): Promise<void> {
    this.setLoading(true);

    const { response }: ApiResponse<ApiConcertType> = await api(
      `${urls.concertGet}?id=${id}`
    );

    if (response) {
      runInAction(() => {
        this.currentConcert = normalizeConcert(response);
      });
    }

    this.setLoading(false);
  }

  fetchConcertsList = async (withRewrite = false): Promise<void> => {
    if (withRewrite) {
      this.setLoading(true);

      runInAction(() => {
        this.currentConcertsOffset = 0;
        this.concerts = [];

        this.currentConcertsOffset = 0;
      });
    }

    const { response }: ApiListResponseType<ApiConcertType> = await api(
      urls.concertList,
      'GET',
      {
        offset: this.currentConcertsOffset,
        limit: CONCERTS_LIST_LIMIT,
      }
    );

    if (response) {
      runInAction(() => {
        this.concerts = withRewrite
          ? response.items.map(normalizeConcert)
          : [...this.concerts, ...response.items.map(normalizeConcert)];

        this.concertsTotal = response.count;

        this.currentConcertsOffset += CONCERTS_LIST_LIMIT;
      });
    }

    this.setLoading(false);
  };

  searchConcerts = async (query: string): Promise<void> => {
    runInAction(() => {
      this.isSearching = true;
    });

    const { response }: ApiListResponseType<ApiConcertType> = await api(
      urls.concertList,
      'GET',
      {
        query,
      }
    );

    if (response) {
      runInAction(() => {
        this.concertsSearchResult = response.items?.map(normalizeConcert) || [];
      });
    }

    runInAction(() => {
      this.isSearching = false;
    });
  };
}
