import { CommonDataStore } from './CommonDataStore';
import { UserStore } from './UserStore';

export class RootStore {
  commonDataStore = new CommonDataStore(this);
  userStore = new UserStore(this);
}
