import { makeAutoObservable, runInAction } from 'mobx';

import urls from 'config/urls';
import { normalizeConcert, normalizeUser } from 'utils/normalizers';
import api, { ApiResponse } from 'utils/api';

import { RootStore } from './RootStore';
import {
  ApiConcertType,
  ApiListResponseType,
  ApiUserType,
  AudiosStatus,
  ConcertsByDateEnum,
  ConcertType,
  ShortConcertType,
  UserType,
} from './types';

const CONCERTS_LIST_LIMIT = 20;

export class UserStore {
  rootStore: RootStore;

  currentUser: UserType;

  usersComingConcerts: ShortConcertType[] = [];
  usersComingConcertsCount: number | null = null;

  usersPassedConcerts: ShortConcertType[] = [];
  usersPassedConcertsCount: number | null = null;

  recommendedUsers: UserType[] = [];
  recommendedUsersCount = 0;

  recommendedConcerts: ConcertType[] = [];
  recommendedConcertsCount: number | null = null;

  currentConcertsOffset = 0;

  isAuthCheckLoading = true;
  isAuthorized = false;
  isLoading = false;

  constructor(rootStore: RootStore) {
    makeAutoObservable(this, {
      rootStore: false,
    });

    this.rootStore = rootStore;
  }

  /* --- getters --- */

  get isProcessing(): boolean {
    return this.currentUser?.audiosStatus === AudiosStatus.processing;
  }

  get isBlocked(): boolean {
    return this.currentUser?.audiosStatus === AudiosStatus.blocked;
  }

  get hasAccessToAudios(): boolean {
    return !this.isBlocked && !this.isProcessing;
  }

  /* --- setters --- */

  setLoading(value: boolean): void {
    this.isLoading = value;
  }

  /* ----- api ----- */

  fetchCurrentUser = async (): Promise<void> => {
    runInAction(() => {
      this.isAuthCheckLoading = true;
    });

    const { response }: ApiResponse<ApiUserType> = await api(urls.userCurrent);

    if (response) {
      runInAction(() => {
        this.currentUser = normalizeUser(response);
        this.isAuthorized = true;
      });
    }

    runInAction(() => {
      this.isAuthCheckLoading = false;
    });
  };

  fetchUserConcerts = async (
    dateFilter = ConcertsByDateEnum.coming
  ): Promise<void> => {
    this.setLoading(true);

    const { response }: ApiListResponseType<ApiConcertType> = await api(
      urls.concertList,
      'GET',
      {
        user_id: this.currentUser.id,
        date_filter: dateFilter,
      }
    );

    if (response) {
      runInAction(() => {
        if (dateFilter === ConcertsByDateEnum.coming) {
          this.usersComingConcerts =
            response.items?.map(normalizeConcert) || [];
          this.usersComingConcertsCount = response.count;
        } else if (dateFilter === ConcertsByDateEnum.passed) {
          this.usersPassedConcerts =
            response.items?.map(normalizeConcert) || [];
          this.usersPassedConcertsCount = response.count;
        }
      });
    }

    this.setLoading(false);
  };

  sendConcertToggle = async (concertId: number): Promise<void> => {
    const { response }: ApiResponse<ApiConcertType> = await api(
      urls.concertToggle,
      'POST',
      {
        concert_id: concertId,
      }
    );

    if (response) {
      runInAction(() => {
        response.is_going
          ? this.currentUser.concertsCount++
          : this.currentUser.concertsCount--;

        if (this.rootStore.commonDataStore.currentConcert?.id === concertId) {
          this.rootStore.commonDataStore.currentConcert.isGoing =
            response.is_going;

          if (response.is_going) {
            this.rootStore.commonDataStore.currentConcert.users.push(
              this.currentUser
            );
          } else {
            this.rootStore.commonDataStore.currentConcert.users = this.rootStore.commonDataStore.currentConcert.users.filter(
              ({ id }) => id !== this.currentUser.id
            );
          }
        }
      });
    }
  };

  fetchRecommendedUsers = async (
    withLoading = true,
    limit = 10,
    offset = 0
  ): Promise<void> => {
    if (withLoading) {
      this.setLoading(true);
    }

    const { response }: ApiListResponseType<ApiUserType> = await api(
      urls.recommendedUsers,
      'GET',
      {
        limit,
        offset,
      }
    );

    if (response) {
      runInAction(() => {
        this.recommendedUsers = response.items?.map(normalizeUser) || [];
        this.recommendedUsersCount = response.count;
      });
    }

    if (withLoading) {
      this.setLoading(false);
    }
  };

  fetchRecommendedConcerts = async (
    withLoading = true,
    offset = 0,
    limit = 10
  ): Promise<void> => {
    if (withLoading) {
      this.setLoading(true);
    }

    const { response }: ApiListResponseType<ApiConcertType> = await api(
      urls.recommendedConcerts,
      'GET',
      {
        offset,
        limit,
      }
    );

    if (response) {
      runInAction(() => {
        this.recommendedConcerts = response.items?.map(normalizeConcert) || [];
        this.recommendedConcertsCount = response.count;
      });
    }

    if (withLoading) {
      this.setLoading(false);
    }
  };

  fetchMainPageData = async (): Promise<void> => {
    this.setLoading(true);

    await Promise.all([
      this.fetchRecommendedUsers(false),
      this.fetchRecommendedConcerts(false),
    ]);

    this.setLoading(false);
  };

  fetchRecommendedConcertsList = async (withRewrite = false): Promise<void> => {
    if (withRewrite) {
      this.setLoading(true);

      runInAction(() => {
        this.recommendedConcertsCount = 0;
        this.recommendedConcerts = [];

        this.currentConcertsOffset = 0;
      });
    }

    const { response }: ApiListResponseType<ApiConcertType> = await api(
      urls.recommendedConcerts,
      'GET',
      {
        offset: this.currentConcertsOffset,
        limit: CONCERTS_LIST_LIMIT,
      }
    );

    if (response) {
      runInAction(() => {
        this.recommendedConcerts = withRewrite
          ? response.items.map(normalizeConcert)
          : [
              ...this.recommendedConcerts,
              ...response.items.map(normalizeConcert),
            ];

        this.recommendedConcertsCount = response.count;

        this.currentConcertsOffset += CONCERTS_LIST_LIMIT;
      });
    }

    this.setLoading(false);
  };

  sendLogout = async (): Promise<void> => {
    if (this.isAuthorized) {
      const { response } = await api(urls.userLogout);

      if (response) {
        runInAction(() => {
          this.isAuthorized = false;
        });
      }
    }
  };
}
