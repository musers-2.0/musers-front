import { MobXProviderContext } from 'mobx-react';
import { useContext } from 'react';

import { RootStore } from './RootStore';
import { CommonDataStore } from './CommonDataStore';
import { UserStore } from './UserStore';

export function useStore(): RootStore {
  return useContext(MobXProviderContext).rootStore as RootStore;
}

export function useCommonDataStore(): CommonDataStore {
  return useStore().commonDataStore;
}

export function useUserStore(): UserStore {
  return useStore().userStore;
}
