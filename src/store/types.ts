import { ApiResponse } from '../utils/api';

export type ApiListResponseType<T> = ApiResponse<{ items: T[]; count: number }>;

export type PlaceType = {
  title: string;
  address: string;
  subway: string;
  location: string;
  latitude: number;
  longitude: number;
};

export type ApiConcertType = {
  id: number;
  place: PlaceType | null;
  tags: string[];
  kudago_id: number;
  title: string;
  short_title: string;
  description: string;
  price: number;
  start_date: string;
  image_url: string;
  users_count: number;
  users: ApiUserType[];
  is_going?: boolean;
};

export type ShortConcertType = {
  id: number;
  title: string;
  place: PlaceType | null;
  startDate: string;
  image: string;
  usersCount: number;
  users: UserType[];
  shortTitle: string;
};

export type ConcertType = ShortConcertType & {
  tags: string[];
  description: string;
  price: number;
  isGoing?: boolean;
};

export type ApiUserType = {
  id: number;
  first_name: string;
  last_name: string;
  avatar: string;
  domain: string;
  age?: number;
  audios_status?: AudiosStatus;
  concerts_count: number;
};

export type UserType = {
  id: number;
  firstName: string;
  lastName?: string;
  avatar: string;
  domain?: string;
  age?: number;
  audiosStatus?: AudiosStatus;
  concertsCount: number;
};

export enum ConcertsByDateEnum {
  passed = 'passed',
  coming = 'coming',
}

export enum AudiosStatus {
  blocked = 'blocked',
  processing = 'proccessing',
  processed = 'proccessed',
}
