import { css } from 'styled-components';
import { COLORS } from 'styles/vars';

export const gradientText = (color) => css`
  background: ${color};
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
`;

export const gradientOverlay = css`
  position: relative;
  overflow: hidden;

  &::before {
    content: '';
    position: absolute;
    width: 100%;
    height: 100%;
    background: linear-gradient(
      180deg,
      rgba(0, 0, 0, 0) 0%,
      rgba(32, 34, 41, 0.65) 74.86%
    );
    transition: opacity 0.3s linear;
  }
`;

export const bottomGradientOverlay = css`
  position: relative;

  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 200%;
    background: linear-gradient(180deg, rgba(41, 44, 51, 0.36) 0, #202229 49%);
  }
`;

export const scrollbarMixin = css`
  &::-webkit-scrollbar {
    width: 10px;
  }

  &::-webkit-scrollbar-track {
    background: ${COLORS.BLACK};
    border-radius: 10px;
  }

  &::-webkit-scrollbar-thumb {
    background: ${COLORS.SURFACE};
    border-radius: 10px;

    &:hover {
      background: ${COLORS.SURFACE};
    }
  }
`;
