export const COLORS = {
  // theme
  BACKGROUND: '#202229',
  SURFACE: '#292C33',
  TEXT: '#FFFFFF',
  DIM_TEXT: 'rgba(255, 255, 255, 0.7)',
  // common colors
  GRAY: '#848383',
  ORANGE: '#FF835F',
  PINK: '#FF7798',
  BLACK: '#17191F',
  DARK_GRAY: 'rgba(41, 44, 51, 0.5)',
  TRANSPARENT_GRAY: 'rgba(0, 0, 0, 0.3)',
};

export const GRADIENTS = {
  PINK_RADIAL:
    'radial-gradient(87.92% 244.7% at 86.88% 12.08%, #FFB570 3.74%, #FF7798 100%)',
  PINK_LINEAR: 'linear-gradient(90deg, #051EFF 0%, #FF8C90 100%)',
  ORANGE_RADIAL: 'linear-gradient(180deg, #FF835F 0%, #FF7573 100%)',
  SMOKE_GRADIENT:
    'linear-gradient(180deg, rgba(41, 44, 51, 0) 0%, #292c33 90%)',
};

export const MARGINS = {
  SIDE: '10rem',
  BLOCK_END: '7rem',
};

export const ANIMATIONS = {
  TEXT_HOVER: `
    transition: color 0.3s linear;
    
    &:hover {
      color: white;
    }
  `,
  GRADIENT_HOVER: `
    transition: all 0.3s linear;
    
    &:hover {
      background-position: 50% 0;
    }
  `,
  ORANGE_TO_PINK_HOVER: `
    color: ${COLORS.ORANGE};
    transition: color 0.3s linear;

    &:hover {
      color: ${COLORS.PINK};
    }
  `,
  SCALE_HOVER: `
    transition: transform 0.3s linear;

    &:hover {
      transform: scale(1.1);
    }
  `,
};
