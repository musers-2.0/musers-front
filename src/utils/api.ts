import axios, { AxiosRequestConfig, AxiosResponse, Method } from 'axios';

export type ApiResponse<T> = {
  response?: T;
  error?: any;
  errorData?: any;
};

export function callApi(
  endpoint: string,
  method: Method = 'GET',
  config: AxiosRequestConfig = {}
): Promise<any> {
  const url = `${window.location.origin}/${endpoint}`;

  return axios({
    method,
    url,
    ...config,
  }).then(
    (result: AxiosResponse<any>) => {
      if (result.status !== 200 && result.data?.status !== 'ok') {
        return Promise.reject(result);
      }

      if (result.data?.status === 'error') {
        return Promise.reject(result);
      }

      const response = result.data?.data || result.data || result;

      return { response, headers: result.headers };
    },
    (error) => {
      return Promise.reject(error);
    }
  );
}

export default function api(
  endpoint: string,
  method: Method = 'GET',
  data = {},
  config: AxiosRequestConfig = {},
  multipartFormData = false
): Promise<ApiResponse<any>> {
  const queryConfig = { ...config };

  if (
    (queryConfig.data === null || queryConfig.data === undefined) &&
    method !== 'GET'
  ) {
    queryConfig.data = data;
  }

  if (
    (queryConfig.params === null || queryConfig.params === undefined) &&
    method === 'GET'
  ) {
    queryConfig.params = data;
  }

  if (!queryConfig.headers) {
    queryConfig.headers = {};
  }

  if (multipartFormData) {
    const formData = new FormData();
    Object.keys(data).forEach((key) => {
      const value = data[key];
      if (Array.isArray(value)) {
        value.forEach((item) => {
          formData.append(key, item);
        });
      } else {
        formData.append(key, value);
      }
    });
    queryConfig.data = formData;

    Object.assign(queryConfig.headers, {
      'Content-Type': 'multipart/form-data',
    });
  }

  return callApi(endpoint, method, queryConfig)
    .then(({ response, headers }) => ({
      response,
      headers,
      error: null,
      errorData: null,
    }))
    .catch((error) => {
      return {
        response: null,
        error,
        errorData: error.response?.data || {},
      };
    });
}
