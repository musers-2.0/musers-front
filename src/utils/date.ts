import dayjs from 'dayjs';
import 'dayjs/locale/ru';

const getDate = (date: string): dayjs.Dayjs => dayjs(date);

export const getDayMonthAndTime = (time: string): string => {
  return getDate(time).locale('ru').format('D MMMM, HH:mm');
};

export const getDayAndMonth = (time: string): string => {
  return getDate(time).locale('ru').format('D MMMM');
};

export const getDaysLeft = (time: string): number => {
  return getDate(time).diff(dayjs(), 'days');
};
