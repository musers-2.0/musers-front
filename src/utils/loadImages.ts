export const loadImage = async (src: string, onLoaded: () => void) =>
  new Promise((resolve: (value?: any) => void) => {
    const curImage = new Image();
    curImage.src = src;
    curImage.onload = () => {
      onLoaded();
      resolve();
    };

    curImage.onerror = () => resolve();
  });

export default async (images: Array<string>, onNextLoaded = () => null) => {
  await Promise.all(images.map((i) => loadImage(i, onNextLoaded)));
};
