import {
  ApiConcertType,
  ApiUserType,
  AudiosStatus,
  ConcertType,
  UserType,
} from 'store/types';

export const normalizeConcert = (concert: ApiConcertType): ConcertType => ({
  id: concert.id,
  place: concert.place || null,
  tags: concert.tags,
  title: concert.title,
  shortTitle: concert.short_title,
  description: concert.description,
  price: concert.price,
  startDate: concert.start_date,
  image: concert.image_url,
  usersCount: concert.users_count,
  users: concert.users.map(normalizeUser),
  isGoing: Boolean(concert.is_going),
});

export const normalizeUser = (user: ApiUserType): UserType => ({
  id: user.id,
  firstName: user.first_name,
  lastName: user.last_name,
  avatar: user.avatar,
  domain: user.domain,
  age: user.age,
  audiosStatus: user.audios_status || AudiosStatus.processed,
  concertsCount: user.concerts_count,
});
