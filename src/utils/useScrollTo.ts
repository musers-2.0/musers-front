import * as React from 'react';

export default (): [ React.RefObject<HTMLInputElement>, () => void ] => {
  const elementRef = React.useRef<HTMLInputElement>(null);
  const scrollTo = React.useCallback(() => {
    if (elementRef.current) {
      elementRef.current.scrollIntoView({
        behavior: 'smooth',
      });
    }
  }, [elementRef]);

  return [ elementRef, scrollTo ];
};
