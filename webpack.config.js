const path = require('path');

const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const HtmlWepbackPlugin = require('html-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const src = path.resolve(__dirname, 'src');
const build = path.resolve(__dirname, 'public');

const getCSSLoader = (withModules = false) => [
  {
    loader: MiniCssExtractPlugin.loader,
  },
  {
    loader: 'css-loader',
    options: {
      modules: withModules && {
        localIdentName: '[name]__[local]__[fullhash:base64:5]',
      },
    },
  },
  {
    loader: 'postcss-loader',
    options: {
      postcssOptions: {
        plugins: () => [autoprefixer()],
      },
    },
  },
  {
    loader: 'sass-loader',
  },
];

module.exports = {
  context: src,
  entry: './index.tsx',
  output: {
    path: build,
    publicPath: '/',
    filename: 'static/js/bundle.[fullhash].js',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx'],
    alias: {
      components: path.join(src, 'components'),
      img: path.join(src, 'img'),
      pages: path.join(src, 'pages'),
      store: path.join(src, 'store'),
      styles: path.join(src, 'styles'),
      utils: path.join(src, 'utils'),
      config: path.join(src, 'config'),
    },
  },
  module: {
    rules: [
      {
        test: /\.(ts|js)x?$/,
        loader: 'babel-loader',
        exclude: '/node_modules/',
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        exclude: /\.(component|c)\.svg$/,
        type: 'asset',
        generator: {
          filename: 'static/img/[name].[contenthash][ext]',
        },
        parser: {
          dataUrlCondition: {
            maxSize: 8192,
          },
        },
      },
      {
        test: /\.(component|c)\.svg$/,
        use: [
          {
            loader: '@svgr/webpack',
            options: {
              memo: true,
            },
          },
        ],
      },
      {
        test: /\.(eot|woff2|woff|ttf?)$/,
        type: 'asset',
        generator: {
          filename: 'static/fonts/[name].[contenthash][ext]',
        },
      },
      {
        test: /\.s?css$/,
        exclude: /\.modules\.(s?css|sass)$/,
        use: getCSSLoader(false),
      },
      {
        test: /\.modules\.(s?css|sass)$/,
        use: getCSSLoader(true),
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      },
    }),
    new HtmlWepbackPlugin({
      filename: 'index.html',
      template: path.join(src, 'index.html'),
    }),
    new MiniCssExtractPlugin({
      filename: 'static/css/bundle.[name].[contenthash].css',
    }),
    new ForkTsCheckerWebpackPlugin({
      typescript: {
        configFile: path.resolve(__dirname, 'tsconfig.json'),
      },
      eslint: {
        enabled: true,
        files: '*.{ts,tsx,js,jsx}',
      },
    }),
    new CopyPlugin({
      patterns: [
        {
          from: path.join(src, 'img', 'favicon'),
          to: path.join(build, 'static', 'img', 'favicon'),
        },
      ],
    }),
  ],
  devServer: {
    host: '0.0.0.0',
    historyApiFallback: true,
    inline: true,
    hot: true,
    https: true,
    proxy: {
      '/api': {
        changeOrigin: true,
        target: 'https://musers.me',
        secure: true,
        debug: true,
      },
    },
  },
};
